package net.impacto.magazine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
@ComponentScan(basePackages = {"net.impacto.magazine"})
public class IevanMagazineWsApplication {
	public static void main(String[] args) {
		SpringApplication.run(IevanMagazineWsApplication.class, args);
	}
}
