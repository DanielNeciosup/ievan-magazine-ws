package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListTagResponse;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.TagService;

@RestController
@RequestMapping("/tags")
public class TagController {
    @Autowired
    private ResponseService responseService;
	
	@Autowired
	private TagService tagService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListTagResponse> listTags(@RequestHeader("username") String username,
												  @RequestHeader("token") String token,
												  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
			 									  @RequestParam(value="tagId", required = false) Integer tagId,
			 									  @RequestParam(value="noteId", required = false) Integer noteId) 
	{
		BaseResponse<ListTagResponse> response = new BaseResponse<ListTagResponse>();
		var request = new FiltersTagsRequest();

		request.setUsername(username);
        request.setLang(lang);
		request.setNoteId(noteId);
		request.setTagId(tagId);
		
		try {
			response = tagService.listTags(request);
		}catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
}
