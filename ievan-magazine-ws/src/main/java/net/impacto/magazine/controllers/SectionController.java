package net.impacto.magazine.controllers;

import net.impacto.magazine.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListSectionsResponse;
import net.impacto.magazine.service.SectionService;

@RestController
@RequestMapping("/sections")
public class SectionController {
	@Autowired
	private ResponseService responseService;

	@Autowired
	private SectionService sectionService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListSectionsResponse> listSections(@RequestHeader("username") String username,
														   @RequestHeader("token") String token,
														   @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
														   @RequestParam(value = "sectionId", required = false) Integer sectionId) 
	{
		BaseResponse<ListSectionsResponse> response = new BaseResponse<ListSectionsResponse>();
		var request = new FiltersSectionsRequest();

		request.setUsername(username);
        request.setLang(lang);
        request.setSectionId(sectionId);

		try {
			response = sectionService.listSections(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}

		return response;
	}
}
