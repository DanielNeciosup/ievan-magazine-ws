package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import net.impacto.magazine.dtos.request.FileBase64NewsRequest;
import net.impacto.magazine.dtos.request.FiltersNewsRequest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNewsResponse;
import net.impacto.magazine.dtos.response.NewsResponse;
import net.impacto.magazine.service.NewsService;
import net.impacto.magazine.service.ResponseService;

@RequestMapping("/news")
@RestController
public class NewsController {
    @Autowired
    private ResponseService responseService;

    @Autowired
    private NewsService newsService;
    
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<ListNewsResponse> listNews(@RequestHeader("username") String username,
    											   @RequestHeader("token") String token,
    											   @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    		 									   @RequestParam(value = "id", required = false) String id,
    		 									   @RequestParam(value = "title", required = false) String title,
    		 									   @RequestParam(value = "description", required = false) String description,
    		 									   @RequestParam(value = "minViews", required = false) Integer minViews,
    		 									   @RequestParam(value = "maxViews", required = false) Integer maxViews,
    		 									   @RequestParam(value = "minPublishedAt", required = false) String minPublishedAt,
    		 									   @RequestParam(value = "maxPublishedAt", required = false) String maxPublishedAt,
    		 									   @RequestParam(value = "isPublished", required = false) Boolean isPublished,
    		 									   @RequestParam(value = "sort", required = false)  String sort,
 								         		   @RequestParam(value = "pageNum", required = false) Integer pageNum,
 								         		   @RequestParam(value = "pageSize", required = false) Integer pageSize)
    {
    	BaseResponse<ListNewsResponse> response = new BaseResponse<>();
    	var request = new FiltersNewsRequest();
    	
    	request.setLang(lang);
    	request.setId(id);
    	request.setTitle(title);
    	request.setDescription(description);
    	request.setMinViews(minViews);
    	request.setMaxViews(maxViews);
    	request.setMinPublishedAt(minPublishedAt);
    	request.setMaxPublishedAt(maxPublishedAt);
    	request.setSort(sort);
    	request.setPageNum(pageNum);
    	request.setPageSize(pageSize);
    	request.setIsPublished(isPublished);
    	
    	try {
            response = newsService.listNews(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(value = "/{newsId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<NewsResponse> getNews(@RequestHeader("username") String username,
    										  @RequestHeader("token") String token,
    										  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
			   								  @PathVariable("newsId") String newsId,
			   								  @RequestParam(value = "updateViews", required = false) Boolean updateViews) {
    	BaseResponse<NewsResponse> response = new BaseResponse<>();
    	var request = new FiltersNewsRequest();
    	
    	request.setLang(lang);
    	request.setId(newsId);
    	request.setUpdateViews(updateViews);
    	
    	try {
            response = newsService.getNews(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(value = "/highlighted",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<NewsResponse> getHighlighted(@RequestHeader("username") String username,
    												 @RequestHeader("token") String token, 
    												 @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang)
    {
    	BaseResponse<NewsResponse> response = new BaseResponse<>();
    	var request = new FiltersNewsRequest();
    	
    	request.setIsHighlighted(true);
    	request.setLang(lang);
    	
    	try 
    	{
            response = newsService.getNews(request);
        } 
    	catch (Exception e) 
    	{
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Create News", notes = "Only send the following properties by the body: news, imgBase64. Exclude others.")
    public BaseResponse<NewsResponse> createNews(@RequestHeader("username") String username,
    											 @RequestHeader("token") String token,
    											 @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    											 @RequestBody NewsRequest request)
    {
    	BaseResponse<NewsResponse> response = new BaseResponse<>();
    	
    	request.setUsername(username);
    	request.setLang(lang);
    	
    	try {
            response = newsService.createNews(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Update News", notes = "Only send the following properties by the body: news. Exclude others.")
    public BaseResponse<NewsResponse> updateNews(@RequestHeader("username") String username,
    											 @RequestHeader("token") String token,
    											 @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    											 @RequestBody NewsRequest request)
    {
    	BaseResponse<NewsResponse> response = new BaseResponse<>();
    	
    	request.setUsername(username);
    	request.setLang(lang);
    	
    	try {
            response = newsService.updateNews(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(value = "/image", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Update News Image", notes = "Only send the following properties by the body: id, file. Exclude others.")
    public BaseResponse<?> updateImage(@RequestHeader("username") String username,
    								   @RequestHeader("token") String token,
    								   @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    								   @RequestBody FileBase64NewsRequest request)
    {
    	BaseResponse<?> response = new BaseResponse<>();
    	
    	request.setUsername(username);
    	request.setLang(lang);
    	
    	try {
            response = newsService.updateImage(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<?> deleteNews(@RequestHeader("username") String username,
    								  @RequestHeader("token") String token,
    								  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
   								      @RequestParam("newsId") String newsId)
    {
    	BaseResponse<?> response = new BaseResponse<>();
    	var request = new FiltersNewsRequest();
    	
    	request.setUsername(username);
    	request.setLang(lang);
    	request.setId(newsId);
    	
    	try {
            response = newsService.deleteNews(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }
    	
    	return response;
    }
    
    @RequestMapping(value = "/highlighted", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<?> hightlightNews(@RequestHeader("username") String username,
    									  @RequestHeader("token") String token,
										  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
										  @RequestBody HighlightedNewsRequest request)
    {
    	BaseResponse<?> response = new BaseResponse<>();
    	
    	request.setUsername(username);
    	request.setLang(lang);

    	try {
			response = newsService.hightlightNews(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
    	
    	return response;
    }
}
