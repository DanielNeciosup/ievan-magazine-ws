package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListZonesResponse;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.ZoneService;

@RestController
@RequestMapping("/zones")
public class ZoneController {
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private ZoneService zoneService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListZonesResponse> listZone(@RequestHeader("userName") String username,
													@RequestHeader("token") String token,
													@RequestHeader("Accept-Language") String acceptLanguage,
													@RequestParam(value = "isActive", required = false) Boolean isActive
			) {
		FiltersZoneRequest request = new FiltersZoneRequest();
		request.setIstrue(isActive);
		BaseResponse<ListZonesResponse> response = new BaseResponse<>();
		try {
			response = zoneService.listZones(request);
		}catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/editions/{editionId}/zones", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListZonesResponse> listZoneLanguagesEdition(@RequestHeader("userName") String username,
													@RequestHeader("token") String token,
													@RequestHeader("Accept-Language") String acceptLanguage,
													@PathVariable("editionId") String editionId
			) {
		
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		request.setEditionIds(editionId);
		BaseResponse<ListZonesResponse> response = new BaseResponse<>();
		try {
			response = zoneService.listLanguageZonesEditions(request); 
		}catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
}
