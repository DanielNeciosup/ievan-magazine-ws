package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.service.ContentService;
import net.impacto.magazine.service.ResponseService;

@RestController
@RequestMapping("/rootContents")
public class ContentController {
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private ContentService contentService;
	
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> insertRootContent(   	 @RequestHeader("UserName") String userName,
																 @RequestHeader("token") String token,
																 @RequestHeader("Accept-Language") String acceptLanguage,
																 @RequestBody ContentRequest request){
		BaseResponse<?> response = new BaseResponse<>();
		request.setLang(acceptLanguage);
		request.setUsername(userName);
		try {
			response = contentService.insertContent(request); 
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
}
