package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListFileFormatsResponse;
import net.impacto.magazine.service.FileFormatService;
import net.impacto.magazine.service.ResponseService;

@RestController
@RequestMapping("/fileFormats")
public class FileFormatController {
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private FileFormatService fileFormat;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListFileFormatsResponse> listFileFormat( 		@RequestHeader("userName") String username,
																		@RequestHeader("token") String token,
																		@RequestHeader("Accept-Language") String acceptLanguage,
																		@RequestParam(value = "isActive", required = false) Boolean isActive
			){
		FiltersFileFormatRequest request = new FiltersFileFormatRequest();
		request.setIstrue(isActive);
		BaseResponse<ListFileFormatsResponse> response = new BaseResponse<>();
		try {
			response = fileFormat.listFileFormat(request);
		}catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
}
