package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import net.impacto.magazine.dtos.request.ConsolidatedRequest;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.ListConsolidatedRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ConsolidatedResponse;
import net.impacto.magazine.dtos.response.EditionResponse;
import net.impacto.magazine.dtos.response.ListContentResponse;
import net.impacto.magazine.dtos.response.ListEditionsResponse;
import net.impacto.magazine.dtos.response.ListZonesResponse;
import net.impacto.magazine.entity.FileLink;
import net.impacto.magazine.service.EditionService;
import net.impacto.magazine.service.ResponseService;

@RestController
@RequestMapping("/editions")
public class EditionController {
	@Autowired
	private ResponseService responseService;

	@Autowired
	private EditionService editionService;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<EditionResponse> registerEdition(@RequestHeader("UserName") String userName,
														 @RequestHeader("token") String token,
														 @RequestHeader("Accept-Language") String acceptLanguage,
														 @RequestBody EditionRequest request) {
		BaseResponse<EditionResponse> response = new BaseResponse<EditionResponse>();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		try {
			response = editionService.insertEdition(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/mainData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<EditionResponse> registerMainEdition(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") String editionId,
																@RequestBody EditionRequest request) {
		BaseResponse<EditionResponse> response = new BaseResponse<EditionResponse>();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		try {
			response = editionService.registerMainEdition(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}

	@RequestMapping(value = "/{editionId}/consolidated", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> updateConsolidatedEdition(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestParam(value = "lang", defaultValue = "es") String acceptLanguage,
																@PathVariable("editionId") String editionId,
																@RequestBody ListConsolidatedRequest request) {
		BaseResponse<?> response = new BaseResponse<>();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		try {
			response = editionService.updateConsolidatedEdition(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/content", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> registerContentEdition(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") String editionId,
																@RequestBody ContentRequest request) {
		BaseResponse<?> response = new BaseResponse<>();
		request.setUsername(userName);
		try {
			response = editionService.insertContentEdition(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	// SEPARADOR -----
	
	
	
	
	@RequestMapping(value = "/{editionId}/consolidated", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ConsolidatedResponse> deleteConsolidatedEdition(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") String editionId,
																@RequestBody ConsolidatedRequest request) {
		BaseResponse<ConsolidatedResponse> response = new BaseResponse<ConsolidatedResponse>();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListEditionsResponse> listEditions(@RequestHeader("userName") String username,
														   @RequestHeader("token") String token,
														   @RequestHeader("Accept-Language") String acceptLanguage,
														   @RequestParam(value = "editionIds", required = false) String editionIds,
														   @RequestParam(value = "pageSize", required = false) Integer pageSize,
														   @RequestParam(value = "pageNum", required = false) Integer pageNum,
														   @RequestParam(value = "lang", required = true, defaultValue = "es") String lang,
														   @RequestParam(value = "sort", required = false) String sort
		) {
		BaseResponse<ListEditionsResponse> response = new BaseResponse<ListEditionsResponse>();
		var request = new FiltersEditionsRequest();
		request.setEditionIds(editionIds);
		request.setPageSize(pageSize);
		request.setPageNum(pageNum);
		request.setUsername(username);
		request.setIdLanguage(lang);
		request.setSortFilters(sort);
		request.setLang(acceptLanguage);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<EditionResponse> 			 getEdition(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") Integer editionId) {
		BaseResponse<EditionResponse> response = new BaseResponse<EditionResponse>();
		var request = new EditionRequest();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		request.getEdition().setId(editionId);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/last", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListEditionsResponse> 		 getLastEdition(@RequestHeader("userName") String userName,
																	@RequestHeader("Accept-Language") String acceptLanguage,
																	@RequestHeader("token") String token) {
		BaseResponse<ListEditionsResponse> response = new BaseResponse<>();
		var request = new EditionRequest();
		request.setUsername(userName);
		request.getEdition().setLang(acceptLanguage);
		try {

		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ConsolidatedResponse> archivedEdition(  @RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") Integer editionId) {
		BaseResponse<ConsolidatedResponse> response = new BaseResponse<ConsolidatedResponse>();
		var request = new EditionRequest();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		request.getEdition().setId(editionId);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}

	@RequestMapping(value = "/{editionId}/zones", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListZonesResponse> 		 getEditionZone(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") Integer editionId) {
		BaseResponse<ListZonesResponse> response = new BaseResponse<ListZonesResponse>();
		var request = new EditionRequest();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		request.getEdition().setId(editionId);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/content", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListContentResponse> 	  	 getContent(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("editionId") Integer editionId) {
		BaseResponse<ListContentResponse> response = new BaseResponse<ListContentResponse>();
		var request = new EditionRequest();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		request.getEdition().setId(editionId);
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/content/{contentId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> 						archivedContent(@RequestHeader("userName") String userName,
																@RequestHeader("token") String token,
																@RequestHeader("Accept-Language") String acceptLanguage,
																@PathVariable("contentId") Integer contentId,
																@PathVariable("editionId") Integer editionId) {
		BaseResponse<?> response = new BaseResponse<>();
		var request = new ContentRequest();
		request.setUsername(userName);
		request.setLang(acceptLanguage);
		
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	
	@RequestMapping(value = "/{editionId}/mainData", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<FileLink> insertMainData(   @RequestHeader("userName") String username,
													@RequestHeader("token") String token,
													@RequestHeader("Accept-Language") String acceptLanguage,
													@PathVariable("editionId") Integer editionId) {
		BaseResponse<FileLink> response = new BaseResponse<FileLink>();
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}

	@RequestMapping(value = "/{editionId}/content/{contentId}/hightlighted", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<FileLink> updateHightlighted(   @RequestHeader("userName") String username,
														@RequestHeader("token") String token,
														@RequestHeader("Accept-Language") String acceptLanguage,
														@PathVariable("editionId") Integer editionId) {
		BaseResponse<FileLink> response = new BaseResponse<FileLink>();
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/zones/{zoneId}/languages/{languageId}/archived", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> 		        archivedSample(   @RequestHeader("userName") String username,
														  @RequestHeader("token") String token,
														  @RequestHeader("Accept-Language") String acceptLanguage,
														  @PathVariable("zoneId") Integer zoneId,
														  @PathVariable("languageId") Integer languageId,
														  @PathVariable("editionId") Integer editionId) {
		BaseResponse<?> response = new BaseResponse<>();
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/zones/{zoneId}/languages/{languageId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> 		          deleteSample(   @RequestHeader("userName") String username,
														  @RequestHeader("token") String token,
														  @RequestHeader("Accept-Language") String acceptLanguage,
														  @PathVariable("zoneId") Integer zoneId,
														  @PathVariable("languageId") Integer languageId,
														  @PathVariable("editionId") Integer editionId) {
		BaseResponse<?> response = new BaseResponse<>();
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/{editionId}/zones/{zoneId}/languages/{languageId}/published", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> 		    publishEditionZone(   @RequestHeader("userName") String username,
														  @RequestHeader("token") String token,
														  @RequestHeader("Accept-Language") String acceptLanguage,
														  @PathVariable("zoneId") Integer zoneId,
														  @PathVariable("languageId") Integer languageId,
														  @PathVariable("editionId") Integer editionId) {
		BaseResponse<?> response = new BaseResponse<>();
		try {
			
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	//  
	
	//////////////////////////////////////// SEPARADOR //////////////////////////////////////////
	
	/*
	
	@RequestMapping(value = "/imageHome", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<FileLink> updateImageHome(@RequestHeader("userName") String username,
			@RequestHeader("token") String token,
			@RequestBody FileBase64EditionLanguageRequest request) {
		BaseResponse<FileLink> response = new BaseResponse<FileLink>();
		try {
			//response = editionService.updateImageHome(request); 
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	
	@RequestMapping(value = "/consolidated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<ListFileEditionResponse> listFilesEditions(@RequestHeader("userName") String username,
			@RequestHeader("token") String token,
			@RequestParam(value = "editionId", required = false) Integer editionId,
			@RequestParam(value = "language", required = false) String language
		) {
		BaseResponse<ListFileEditionResponse> response = new BaseResponse<ListFileEditionResponse>();
		var request = new FiltersFileEditionRequest();
		request.setEditionId(editionId);
		request.setLanguage(language);
		try {
			response = editionService.listFilesEditions(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
	

	

	

	
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "updateEdition", notes = "Only send the following properties by the body: id, dtUserRegister, dtPublication, numEdition, txtSummary and txtTitle. Exclude others.")
	public BaseResponse<EditionResponse> updateEdition(@RequestHeader("UserName") String userName,
																@RequestHeader("token") String token,
																@RequestBody EditionRequest request) 
	{
		BaseResponse<EditionResponse> response = new BaseResponse<EditionResponse>();
		request.setUsername(userName);
		try {
			response = editionService.updateEditions(request); 
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}
		return response;
	}
*/
}
