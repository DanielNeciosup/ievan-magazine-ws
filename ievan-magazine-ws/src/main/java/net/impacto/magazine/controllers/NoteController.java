
package net.impacto.magazine.controllers;

import net.impacto.magazine.dtos.request.FiltersNotesRequest;
import net.impacto.magazine.dtos.request.ListTagsNoteRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNotesResponse;
import net.impacto.magazine.dtos.response.NoteResponse;
import net.impacto.magazine.service.NoteService;
import net.impacto.magazine.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notes")
public class NoteController {
    @Autowired
    private ResponseService responseService;

    @Autowired
    private NoteService noteService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<ListNotesResponse> listNotes(@RequestHeader("username") String username,
    												 @RequestHeader("token") String token,
    												 @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    								         		 @RequestParam(value = "id", required = false) String noteId,
    								         		 @RequestParam(value = "sectionId", required = false) Integer sectionId,
    								         		 @RequestParam(value = "flActive", required = false) Boolean flActive,
    								         		 @RequestParam(value = "sort", required = false)  String sort,
    								         		 @RequestParam(value = "pageNum", required = false) Integer pageNum,
    								         		 @RequestParam(value = "pageSize", required = false) Integer pageSize)
    {
    	BaseResponse<ListNotesResponse> response = new BaseResponse<>();
        var request = new FiltersNotesRequest();

        request.setLang(lang);
        request.setNoteId(noteId);
        request.setSectionId(sectionId);
        request.setFlActive(flActive);
        request.setPageNum(pageNum);
        request.setPageSize(pageSize);
        request.setSort(sort);

        try {
            response = noteService.listNotes(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        }

        return response;
    }
    
    @RequestMapping(value = "/{noteId}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<NoteResponse> getNote(@RequestHeader("username") String username,
    										  @RequestHeader("token") String token,
    										  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
			   								  @PathVariable("noteId") String noteId) {
    	BaseResponse<NoteResponse> response = new BaseResponse<>();
    	var request = new FiltersNotesRequest();

    	request.setLang(lang);
    	request.setNoteId(noteId);
    	
    	try {
			response = noteService.getNote(request);
        } catch (Exception e) {
            responseService.onServerError(e, response);
        } 	
    	
    	return response;
    }
    
    @RequestMapping(value = "/{noteId}/tags", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<?> updateTags(@RequestHeader("username") String username,
			  						  @RequestHeader("token") String token,
    								  @RequestHeader(value = "Accept-Language", defaultValue = "es") String lang,
    								  @PathVariable("noteId") Integer noteId,
    								  @RequestBody ListTagsNoteRequest request)
    {
    	BaseResponse<?> response = new BaseResponse<>();
    	
    	request.setUsername(username);
        request.setLang(lang);
    	request.setNoteId(noteId);
    	
    	try 
    	{
			response = noteService.updateTags(request);
		}
    	catch (Exception e)
    	{
    		responseService.onServerError(e, response);
		}
    	
    	return response;
    }
}
