package net.impacto.magazine.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.magazine.dtos.request.FiltersCoverpageSectionRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.CoverpageSectionResponse;
import net.impacto.magazine.service.CoverpageSectionService;
import net.impacto.magazine.service.ResponseService;

@RequestMapping("/coverpageSection")
@RestController
public class CoverpageSectionController {
	@Autowired
	private ResponseService responseService;

	@Autowired
	private CoverpageSectionService cvpSectionService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	private BaseResponse<CoverpageSectionResponse> getCoverpageSection(@RequestHeader("userName") String userName,
																	   @RequestHeader("token") String token, 
																	   @RequestParam(value = "lang", defaultValue = "es") String lang,
																	   @RequestParam("position") Integer position) {
		BaseResponse<CoverpageSectionResponse> response = new BaseResponse<>();
		FiltersCoverpageSectionRequest request = new FiltersCoverpageSectionRequest();
		request.setUsername(userName);
		request.setLang(lang);
		request.setPosition(position);

		try {
			response = cvpSectionService.getCoverpageSection(request);
		} catch (Exception e) {
			responseService.onServerError(e, response);
		}

		return response;
	}
}
