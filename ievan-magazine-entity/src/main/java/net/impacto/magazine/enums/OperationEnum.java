package net.impacto.magazine.enums;

public enum OperationEnum {
	ADD(1), UPDATE(2), DELETE(3);

	private Integer code;

	private OperationEnum(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
