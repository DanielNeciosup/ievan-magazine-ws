package net.impacto.magazine.enums;

public enum ResponseApi {
    OK(1, "OK"), BUSINESS_ERROR(0, "Error de negocio"), SERVER_ERROR(-1, "Error de servidor");

    private Integer code;
    private String message;

    ResponseApi(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
