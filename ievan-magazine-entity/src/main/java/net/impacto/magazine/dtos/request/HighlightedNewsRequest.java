package net.impacto.magazine.dtos.request;

public class HighlightedNewsRequest extends BaseRequest {
	private Integer newsId;
	private Integer cvpsPosition;

	public Integer getNewsId() {
		return newsId;
	}

	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}

	public Integer getCvpsPosition() {
		return cvpsPosition;
	}

	public void setCvpsPosition(Integer cvpsPosition) {
		this.cvpsPosition = cvpsPosition;
	}
}