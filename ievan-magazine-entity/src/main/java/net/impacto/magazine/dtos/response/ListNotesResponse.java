package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Note;

import java.util.List;

public class ListNotesResponse{
    private List<Note> listNotes;

    public List<Note> getListNotes() {
        return listNotes;
    }

    public void setListNotes(List<Note> listNotes) {
        this.listNotes = listNotes;
    }
}
