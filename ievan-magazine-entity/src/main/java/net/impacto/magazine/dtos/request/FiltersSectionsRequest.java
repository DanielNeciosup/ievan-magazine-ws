package net.impacto.magazine.dtos.request;

public class FiltersSectionsRequest extends BaseRequest{
	private Integer sectionId;

	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
}
