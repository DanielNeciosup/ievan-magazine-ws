package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;

public class FileBase64EditionLanguageRequest extends BaseRequest{
	private FileBase64 file;
	private Integer editionId;
	public FileBase64 getFile() {
		return file;
	}
	public void setFile(FileBase64 file) {
		this.file = file;
	}
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
}
