package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;

public class FileBase64NewsRequest extends BaseRequest {
	private String lang;
	private Integer newsId;
	private FileBase64 file;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getNewsId() {
		return newsId;
	}

	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}

	public FileBase64 getFile() {
		return file;
	}

	public void setFile(FileBase64 file) {
		this.file = file;
	}
}
