package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Edition;

public class EditionResponse {
	private Edition edition;

	public Edition getEdition() {
		return edition;
	}

	public void setEdition(Edition edition) {
		this.edition = edition;
	}
}
