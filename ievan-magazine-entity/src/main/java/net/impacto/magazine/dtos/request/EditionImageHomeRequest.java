package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileLink;

public class EditionImageHomeRequest extends BaseRequest{
	private Integer editionId;
	private FileLink file;
	
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public FileLink getFile() {
		return file;
	}
	public void setFile(FileLink file) {
		this.file = file;
	}

}
