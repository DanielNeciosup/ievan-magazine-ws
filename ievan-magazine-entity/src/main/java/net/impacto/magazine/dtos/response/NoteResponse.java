package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Note;

public class NoteResponse{
    private Note note;

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
