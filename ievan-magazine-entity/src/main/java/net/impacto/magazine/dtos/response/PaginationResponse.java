package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Pagination;

public class PaginationResponse{
	private Pagination pagination;

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
