package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Section;

public class SectionResponse {
	private Section section;

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}
}
