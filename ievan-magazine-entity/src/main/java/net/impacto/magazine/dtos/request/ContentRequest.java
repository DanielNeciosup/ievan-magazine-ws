package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.Content;
import net.impacto.magazine.entity.FileBase64;

public class ContentRequest extends BaseRequest {

	private Integer zoneId;
	private String lang;
	private Integer editionId;
	private Content content;
	private FileBase64 image;
	
	public Integer getZoneId() {
		return zoneId;
	}

	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getEditionId() {
		return editionId;
	}

	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}

	public FileBase64 getImage() {
		return image;
	}

	public void setImage(FileBase64 image) {
		this.image = image;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}
}
