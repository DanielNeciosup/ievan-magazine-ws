package net.impacto.magazine.dtos.request;

public class FiltersNewsRequest extends FiltersPaginationRequest {
	private String id;
	private String title;
	private String description;
	private Integer minViews;
	private Integer maxViews;
	private String minPublishedAt;
	private String maxPublishedAt;
	private Boolean updateViews;
	private Boolean isPublished;
	private Boolean isHighlighted;
	private String sort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMinViews() {
		return minViews;
	}

	public void setMinViews(Integer minViews) {
		this.minViews = minViews;
	}

	public Integer getMaxViews() {
		return maxViews;
	}

	public void setMaxViews(Integer maxViews) {
		this.maxViews = maxViews;
	}

	public String getMinPublishedAt() {
		return minPublishedAt;
	}

	public void setMinPublishedAt(String minPublishedAt) {
		this.minPublishedAt = minPublishedAt;
	}

	public String getMaxPublishedAt() {
		return maxPublishedAt;
	}

	public void setMaxPublishedAt(String maxPublishedAt) {
		this.maxPublishedAt = maxPublishedAt;
	}

	public Boolean getUpdateViews() {
		return updateViews;
	}

	public void setUpdateViews(Boolean updateViews) {
		this.updateViews = updateViews;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Boolean getIsHighlighted() {
		return isHighlighted;
	}

	public void setIsHighlighted(Boolean isHighlighted) {
		this.isHighlighted = isHighlighted;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}
