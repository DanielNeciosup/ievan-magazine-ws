package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.News;

public class ListNewsResponse {
	private List<News> listNews;

	public List<News> getListNews() {
		return listNews;
	}

	public void setListNews(List<News> listNews) {
		this.listNews = listNews;
	}
}
