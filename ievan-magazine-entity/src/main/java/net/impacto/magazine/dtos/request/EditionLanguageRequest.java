package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileLink;

public class EditionLanguageRequest extends BaseRequest{
	private Integer editionId;
	private String languageCode ;
	private FileLink file;
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public FileLink getFile() {
		return file;
	}
	public void setFile(FileLink file) {
		this.file = file;
	}
}
