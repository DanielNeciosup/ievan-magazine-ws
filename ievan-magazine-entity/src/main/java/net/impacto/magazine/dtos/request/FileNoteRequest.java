package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;

public class FileNoteRequest extends BaseRequest {
	private Integer noteId;
	private FileBase64 file;

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	
	public FileBase64 getFile() {
		return file;
	}

	public void setFile(FileBase64 file) {
		this.file = file;
	}
}
