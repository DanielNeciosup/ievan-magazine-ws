package net.impacto.magazine.dtos.response;

import java.util.List;
import net.impacto.magazine.entity.Edition;

public class ListEditionsResponse {
	private List<Edition> listEditions;

	public List<Edition> getListEditions() {
		return listEditions;
	}

	public void setListEditions(List<Edition> listEditions) {
		this.listEditions = listEditions;
	}
}
