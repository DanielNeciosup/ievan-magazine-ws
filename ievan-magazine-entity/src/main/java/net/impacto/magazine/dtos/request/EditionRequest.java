package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.Edition;
import net.impacto.magazine.entity.FileBase64;

public class EditionRequest extends BaseRequest {
	
	private FileBase64 homeImage;
	private FileBase64 coverpageImage;
	private Edition edition;
	private Integer zoneId;
	
	public Integer getZoneId() {
		return zoneId;
	}

	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}

	public FileBase64 getHomeImage() {
		return homeImage;
	}

	public void setHomeImage(FileBase64 homeImage) {
		this.homeImage = homeImage;
	}

	public FileBase64 getCoverpageImage() {
		return coverpageImage;
	}

	public void setCoverpageImage(FileBase64 coverpageImage) {
		this.coverpageImage = coverpageImage;
	}

	public Edition getEdition() {
		return edition;
	}

	public void setEdition(Edition edition) {
		this.edition = edition;
	}
}
