package net.impacto.magazine.dtos.request;

public class FiltersEditionsRequest extends FiltersPaginationRequest {
	
	private String editionIds;
	private Boolean isLast;
	private String idLanguage;
	private String sortFilters;
	
	public String getSortFilters() {
		return sortFilters;
	}
	public void setSortFilters(String sortFilters) {
		this.sortFilters = sortFilters;
	}
	public String getIdLanguage() {
		return idLanguage;
	}
	public void setIdLanguage(String idLanguage) {
		this.idLanguage = idLanguage;
	}
	public String getEditionIds() {
		return editionIds;
	}
	public void setEditionIds(String editionIds) {
		this.editionIds = editionIds;
	}
	public Boolean getIsLast() {
		return isLast;
	}
	public void setIsLast(Boolean isLast) {
		this.isLast = isLast;
	}
}
