package net.impacto.magazine.dtos.request;

public class FiltersLanguageRequest {
	private Integer editionId;
	private Integer zoneId;
	
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public Integer getZoneId() {
		return zoneId;
	}
	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}
}
