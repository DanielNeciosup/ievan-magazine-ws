package net.impacto.magazine.dtos.request;

public class FiltersTagsRequest extends BaseRequest {
	private Integer tagId;
	private Integer noteId;

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}

}
