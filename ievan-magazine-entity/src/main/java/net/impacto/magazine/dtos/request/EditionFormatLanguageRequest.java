package net.impacto.magazine.dtos.request;

public class EditionFormatLanguageRequest extends BaseRequest{
	private Integer editionId;
	private String fileFormatExtension;
	private String languageCode;
	private String linkConsolidated;
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public String getFileFormatExtension() {
		return fileFormatExtension;
	}
	public void setFileFormatExtension(String fileFormatExtension) {
		this.fileFormatExtension = fileFormatExtension;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLinkConsolidated() {
		return linkConsolidated;
	}
	public void setLinkConsolidated(String linkConsolidated) {
		this.linkConsolidated = linkConsolidated;
	}
	
}
