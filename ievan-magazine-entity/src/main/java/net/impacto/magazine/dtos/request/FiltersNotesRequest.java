package net.impacto.magazine.dtos.request;

public class FiltersNotesRequest extends FiltersPaginationRequest {
	private String noteId;
	private Integer sectionId;
	private Boolean updateViews;
	private Boolean flActive;
	private String sort;

	public String getNoteId() {
		return noteId;
	}

	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}

	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public Boolean getUpdateViews() {
		return updateViews;
	}

	public void setUpdateViews(Boolean updateViews) {
		this.updateViews = updateViews;
	}

	public Boolean getFlActive() {
		return flActive;
	}

	public void setFlActive(Boolean flActive) {
		this.flActive = flActive;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}
