package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.FileEdition;

public class ListFileEditionResponse {
private List<FileEdition> listFileEdition;

public List<FileEdition> getListFileEdition() {
	return listFileEdition;
}

public void setListFileEdition(List<FileEdition> listFileEdition) {
	this.listFileEdition = listFileEdition;
}
}
