package net.impacto.magazine.dtos.request;

public class FiltersFileFormatRequest {
	private Boolean istrue;

	public Boolean getIstrue() {
		return istrue;
	}

	public void setIstrue(Boolean istrue) {
		this.istrue = istrue;
	}
}
