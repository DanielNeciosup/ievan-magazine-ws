package net.impacto.magazine.dtos.request;

public class FiltersEditionLanguage extends BaseRequest{
	private Integer idEdition; 
	private String pLanguage;
	private String  txtImgCoverpageLink;
	private String txtImgCoverpageName;
	public Integer getIdEdition() {
		return idEdition;
	}
	public void setIdEdition(Integer idEdition) {
		this.idEdition = idEdition;
	}
	public String getpLanguage() {
		return pLanguage;
	}
	public void setpLanguage(String pLanguage) {
		this.pLanguage = pLanguage;
	}
	public String getTxtImgCoverpageLink() {
		return txtImgCoverpageLink;
	}
	public void setTxtImgCoverpageLink(String txtImgCoverpageLink) {
		this.txtImgCoverpageLink = txtImgCoverpageLink;
	}
	public String getTxtImgCoverpageName() {
		return txtImgCoverpageName;
	}
	public void setTxtImgCoverpageName(String txtImgCoverpageName) {
		this.txtImgCoverpageName = txtImgCoverpageName;
	}
	
}
