package net.impacto.magazine.dtos.request;

public class BaseRequest {
	private String username;
	private String lang;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
}
