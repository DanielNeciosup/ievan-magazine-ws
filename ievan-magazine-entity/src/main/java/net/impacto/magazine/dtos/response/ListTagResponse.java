package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.Item;
import java.util.List;

public class ListTagResponse {
	private List<Item> listTag;

	public List<Item> getListTag() {
		return listTag;
	}

	public void setListTag(List<Item> listTag) {
		this.listTag = listTag;
	}
}
