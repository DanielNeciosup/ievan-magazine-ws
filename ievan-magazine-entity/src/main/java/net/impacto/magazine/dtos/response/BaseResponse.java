package net.impacto.magazine.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import net.impacto.magazine.entity.Error;
import net.impacto.magazine.entity.Pagination;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {
	private Integer state;
	private String message;
	private List<Error> listErrors;
	private Pagination pagination;
	private T data;

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Error> getListErrors() {
		return listErrors;
	}

	public void setListErrors(List<Error> listErrors) {
		this.listErrors = listErrors;
	}

	public void addError(Error error) {
		if (this.listErrors == null) {
			this.listErrors = new ArrayList<>();
		}

		this.listErrors.add(error);
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
