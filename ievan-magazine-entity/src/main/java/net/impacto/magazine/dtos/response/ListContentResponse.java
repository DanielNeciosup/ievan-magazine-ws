package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.Content;

public class ListContentResponse {
	private List<Content> listContent;

	public List<Content> getListContent() {
		return listContent;
	}

	public void setListContent(List<Content> listContent) {
		this.listContent = listContent;
	}
}
