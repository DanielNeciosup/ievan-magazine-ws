package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.Language;

public class ListLanguagesResponse {
	private List<Language> listLanguage;

	public List<Language> getListLanguage() {
		return listLanguage;
	}

	public void setListLanguage(List<Language> listLanguage) {
		this.listLanguage = listLanguage;
	}
}
