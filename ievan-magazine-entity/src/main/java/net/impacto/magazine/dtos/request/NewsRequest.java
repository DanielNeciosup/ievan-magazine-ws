package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.News;

public class NewsRequest extends BaseRequest {
	private Boolean isSckeduled;
	private String tagsIds;
	private News news;
	private FileBase64 imgBase64;

	public Boolean getIsSckeduled() {
		return isSckeduled;
	}

	public void setIsSckeduled(Boolean isSckeduled) {
		this.isSckeduled = isSckeduled;
	}

	public String getTagsIds() {
		return tagsIds;
	}

	public void setTagsIds(String tagsIds) {
		this.tagsIds = tagsIds;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public FileBase64 getImgBase64() {
		return imgBase64;
	}

	public void setImgBase64(FileBase64 imgBase64) {
		this.imgBase64 = imgBase64;
	}
}
