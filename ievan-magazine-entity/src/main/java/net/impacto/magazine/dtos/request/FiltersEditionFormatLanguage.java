package net.impacto.magazine.dtos.request;

public class FiltersEditionFormatLanguage extends BaseRequest{
	private Integer idEdition;
	private String fileFormat;
	private String language;
	private String txtConsolidatedLink;
	public Integer getIdEdition() {
		return idEdition;
	}
	public void setIdEdition(Integer idEdition) {
		this.idEdition = idEdition;
	}
	public String getFileFormat() {
		return fileFormat;
	}
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTxtConsolidatedLink() {
		return txtConsolidatedLink;
	}
	public void setTxtConsolidatedLink(String txtConsolidatedLink) {
		this.txtConsolidatedLink = txtConsolidatedLink;
	}
	
	
}
