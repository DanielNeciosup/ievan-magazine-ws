package net.impacto.magazine.dtos.request;

import java.util.List;

import net.impacto.magazine.entity.Item;

public class ListTagsNoteRequest extends BaseRequest {
	private Integer noteId;
	private List<Item> listTags;

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}

	public List<Item> getListTags() {
		return listTags;
	}

	public void setListTags(List<Item> listTags) {
		this.listTags = listTags;
	}
}
