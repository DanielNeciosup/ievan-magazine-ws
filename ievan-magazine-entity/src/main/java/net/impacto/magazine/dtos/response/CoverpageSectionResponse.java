package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.CoverpageSection;

public class CoverpageSectionResponse {
	private CoverpageSection cvpSection;

	public CoverpageSection getCvpSection() {
		return cvpSection;
	}

	public void setCvpSection(CoverpageSection cvpSection) {
		this.cvpSection = cvpSection;
	}
}
