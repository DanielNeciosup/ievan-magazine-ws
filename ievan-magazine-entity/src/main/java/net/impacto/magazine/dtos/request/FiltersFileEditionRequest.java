package net.impacto.magazine.dtos.request;

public class FiltersFileEditionRequest extends BaseRequest{
	private Integer editionId;
	private String language;
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
}
