package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.Section;

public class ListSectionsResponse {
	private List<Section> listSections;

	public List<Section> getListSections() {
		return listSections;
	}

	public void setListSections(List<Section> listSections) {
		this.listSections = listSections;
	}
}
