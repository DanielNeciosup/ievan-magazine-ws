package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.News;

public class NewsResponse {
	private News news;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}
}
