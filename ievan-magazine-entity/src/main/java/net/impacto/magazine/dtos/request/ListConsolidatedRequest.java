package net.impacto.magazine.dtos.request;

import java.util.List;

import net.impacto.magazine.entity.Consolidated;

public class ListConsolidatedRequest extends BaseRequest {
	private Integer editionId;
	private Integer zoneId;
	private String lang;
	private List<Consolidated> listConsolidated;
	public Integer getEditionId() {
		return editionId;
	}
	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}
	public Integer getZoneId() {
		return zoneId;
	}
	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public List<Consolidated> getListConsolidated() {
		return listConsolidated;
	}
	public void setListConsolidated(List<Consolidated> listConsolidated) {
		this.listConsolidated = listConsolidated;
	}
}
