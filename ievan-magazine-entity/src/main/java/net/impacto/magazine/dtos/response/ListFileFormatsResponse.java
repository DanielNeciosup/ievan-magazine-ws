package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.FileFormat;

public class ListFileFormatsResponse {
	private List<FileFormat> listFileFormat;

	public List<FileFormat> getListFileFormat() {
		return listFileFormat;
	}

	public void setListFileFormat(List<FileFormat> listFileFormat) {
		this.listFileFormat = listFileFormat;
	}
}
