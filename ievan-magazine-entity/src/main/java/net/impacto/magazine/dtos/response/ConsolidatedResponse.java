package net.impacto.magazine.dtos.response;

import net.impacto.magazine.entity.FileLink;

public class ConsolidatedResponse {
	private FileLink consolidated;

	public FileLink getConsolidated() {
		return consolidated;
	}

	public void setConsolidated(FileLink consolidated) {
		this.consolidated = consolidated;
	}
}
