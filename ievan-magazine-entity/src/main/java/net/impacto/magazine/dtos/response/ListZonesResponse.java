package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.Zone;

public class ListZonesResponse {
	private List<Zone> listZone;

	public List<Zone> getListZone() {
		return listZone;
	}

	public void setListZone(List<Zone> listZone) {
		this.listZone = listZone;
	}
}
