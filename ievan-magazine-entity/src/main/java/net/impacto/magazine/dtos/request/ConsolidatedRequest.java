package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;

public class ConsolidatedRequest extends BaseRequest {
	private Integer editionId;
	private String format;
	private String lang;
	private Integer zoneId;
	private FileBase64 consolidated;

	public Integer getEditionId() {
		return editionId;
	}

	public void setEditionId(Integer editionId) {
		this.editionId = editionId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getZoneId() {
		return zoneId;
	}

	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}

	public FileBase64 getConsolidated() {
		return consolidated;
	}

	public void setConsolidated(FileBase64 consolidated) {
		this.consolidated = consolidated;
	}
}
