package net.impacto.magazine.dtos.request;

public class FiltersCoverpageSectionRequest extends BaseRequest {
	private Integer position;

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}
}