package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.Note;

public class NoteRequest extends BaseRequest {
	private Note note;
	private FileBase64 imgBase64;
	private String tagsIds;

	public Note getNote() {
		return note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public String getTagsIds() {
		return tagsIds;
	}

	public FileBase64 getImgBase64() {
		return imgBase64;
	}

	public void setImgBase64(FileBase64 imgBase64) {
		this.imgBase64 = imgBase64;
	}

	public void setTagsIds(String tagsIds) {
		this.tagsIds = tagsIds;
	}
}
