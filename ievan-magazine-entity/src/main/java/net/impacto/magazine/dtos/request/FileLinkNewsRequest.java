package net.impacto.magazine.dtos.request;

import net.impacto.magazine.entity.FileLink;

public class FileLinkNewsRequest extends BaseRequest {
	private String lang;
	private Integer newsId;
	private FileLink file;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getNewsId() {
		return newsId;
	}

	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}

	public FileLink getFile() {
		return file;
	}

	public void setFile(FileLink file) {
		this.file = file;
	}
}
