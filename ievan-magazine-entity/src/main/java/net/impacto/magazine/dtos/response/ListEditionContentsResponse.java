package net.impacto.magazine.dtos.response;

import java.util.List;

import net.impacto.magazine.entity.Content;
import net.impacto.magazine.entity.Edition;

public class ListEditionContentsResponse {
    private Edition edition;
	private List<Content> listContent;
    
	public Edition getEdition() {
		return edition;
	}
	
	public void setEdition(Edition edition) {
		this.edition = edition;
	}

	public List<Content> getListContent() {
		return listContent;
	}

	public void setListContent(List<Content> listContent) {
		this.listContent = listContent;
	}
}
