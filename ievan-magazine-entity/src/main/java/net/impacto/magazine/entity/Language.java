package net.impacto.magazine.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Language implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2664000757962585833L;

	private Integer id; 
    private String code;
    private String name;
    private Boolean flActive;
    private Boolean used;
    private Boolean isPublished;
    
	public Boolean getIsPublished() {
		return isPublished;
	}
	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getFlActive() {
		return flActive;
	}
	public void setFlActive(Boolean flActive) {
		this.flActive = flActive;
	}
}
