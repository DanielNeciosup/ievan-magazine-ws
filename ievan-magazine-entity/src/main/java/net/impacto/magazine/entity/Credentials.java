package net.impacto.magazine.entity;

import java.io.Serializable;

public class Credentials implements Serializable {
	private static final long serialVersionUID = 1L;

	private String user;
	private String password;
	private String host;
	private Integer sshPort;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getSshPort() {
		return sshPort;
	}

	public void setSshPort(Integer sshPort) {
		this.sshPort = sshPort;
	}
}
