package net.impacto.magazine.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Zone implements Serializable {

	private static final long serialVersionUID = 2075392003546020621L;

	private Integer id;
	private String name;
	private Boolean isActive;
	private List<Language> listLanguagesUsed;
	private List<Language> listLanguageNotUsed;

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Language> getListLanguagesUsed() {
		return listLanguagesUsed;
	}

	public void setListLanguagesUsed(List<Language> listLanguagesUsed) {
		this.listLanguagesUsed = listLanguagesUsed;
	}

	public List<Language> getListLanguageNotUsed() {
		return listLanguageNotUsed;
	}

	public void setListLanguageNotUsed(List<Language> listLanguageNotUsed) {
		this.listLanguageNotUsed = listLanguageNotUsed;
	}
}
