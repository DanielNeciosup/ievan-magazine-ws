package net.impacto.magazine.entity;

public class StateNodeJSTree {
	private boolean opened;
	
	public StateNodeJSTree() {
		this.opened = true;
	}

	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}
}
