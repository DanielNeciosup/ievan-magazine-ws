package net.impacto.magazine.entity;

import java.io.Serializable;

public class FileEdition implements Serializable{
	
	private static final long serialVersionUID = 374063823060037555L;
	
	private String linkConsolidated;
	private FileType fileType;
	
	
	public String getLinkConsolidated() {
		return linkConsolidated;
	}
	public void setLinkConsolidated(String linkConsolidated) {
		this.linkConsolidated = linkConsolidated;
	}
	public FileType getFileType() {
		return fileType;
	}
	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}
}
