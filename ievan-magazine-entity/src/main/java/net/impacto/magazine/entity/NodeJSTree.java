package net.impacto.magazine.entity;

public class NodeJSTree {
	private StateNodeJSTree state;
	
	public NodeJSTree() {
		this.state = new StateNodeJSTree();
	}

	public StateNodeJSTree getState() {
		return state;
	}

	public void setState(StateNodeJSTree state) {
		this.state = state;
	}
}
