package net.impacto.magazine.entity;

import java.io.Serializable;

public class FileBase64 implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String encodeBase64;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEncodeBase64() {
		return encodeBase64;
	}

	public void setEncodeBase64(String encodeBase64) {
		this.encodeBase64 = encodeBase64;
	}
}
