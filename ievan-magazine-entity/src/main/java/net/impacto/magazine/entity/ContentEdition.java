package net.impacto.magazine.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ContentEdition implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5960013044955002305L;
	
	private Integer id;
	private Integer idContent;
	private Integer idEdition;
	private Integer numLocationPage;
	private String  txtTitle;
	private String  shortDescription;
	private Integer idParent;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdContent() {
		return idContent;
	}
	public void setIdContent(Integer idContent) {
		this.idContent = idContent;
	}
	public Integer getIdEdition() {
		return idEdition;
	}
	public void setIdEdition(Integer idEdition) {
		this.idEdition = idEdition;
	}
	public Integer getNumLocationPage() {
		return numLocationPage;
	}
	public void setNumLocationPage(Integer numLocationPage) {
		this.numLocationPage = numLocationPage;
	}
	public String getTxtTitle() {
		return txtTitle;
	}
	public void setTxtTitle(String txtTitle) {
		this.txtTitle = txtTitle;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public Integer getIdParent() {
		return idParent;
	}
	public void setIdParent(Integer idParent) {
		this.idParent = idParent;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
