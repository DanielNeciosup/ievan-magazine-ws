package net.impacto.magazine.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Note implements Serializable {
	private static final long serialVersionUID = 1433472723379299110L;

	private Integer id;
	private String lang;
	private Integer total;
	private Integer index;
	private String title;
	private Integer views;
	private String description;
	private String content;
	private Boolean isHighLighted;
	private Integer coverpagePosition;
	private Boolean isPublished;
	private String createdAt;
	private String publishedAt;
	private Section section;
	private FileLink mainImage;
	private List<Item> listTags;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getIsHighLighted() {
		return isHighLighted;
	}

	public void setIsHighLighted(Boolean isHighLighted) {
		this.isHighLighted = isHighLighted;
	}

	public Integer getCoverpagePosition() {
		return coverpagePosition;
	}

	public void setCoverpagePosition(Integer coverpagePosition) {
		this.coverpagePosition = coverpagePosition;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public FileLink getMainImage() {
		return mainImage;
	}

	public void setMainImage(FileLink mainImage) {
		this.mainImage = mainImage;
	}

	public List<Item> getListTags() {
		return listTags;
	}

	public void setListTags(List<Item> listTags) {
		this.listTags = listTags;
	}
}
