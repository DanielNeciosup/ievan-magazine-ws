package net.impacto.magazine.entity;

import java.io.Serializable;

public class Pagination implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer totalElements;
	private Integer totalPages;
	private Integer currentPage;
	private String nextLink;
	private String beforeLink;

	public Integer getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Integer totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public String getNextLink() {
		return nextLink;
	}

	public void setNextLink(String nextLink) {
		this.nextLink = nextLink;
	}

	public String getBeforeLink() {
		return beforeLink;
	}

	public void setBeforeLink(String beforeLink) {
		this.beforeLink = beforeLink;
	}
}
