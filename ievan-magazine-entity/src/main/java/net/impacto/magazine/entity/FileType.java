package net.impacto.magazine.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class FileType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5040693049032041836L;
	private Integer id;
	private String extension;
	private String name;
	private Boolean fl_active;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getFl_active() {
		return fl_active;
	}
	public void setFl_active(Boolean fl_active) {
		this.fl_active = fl_active;
	}
	
}
