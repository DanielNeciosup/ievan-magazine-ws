package net.impacto.magazine.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties( ignoreUnknown = true )
@JsonInclude( value = JsonInclude.Include.NON_NULL )
public class Error implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer state;
    private String code;
    private String message;

    public Error() {
    }

    public Error(Integer state, String message) {
        this.state = state;
        this.message = message;
    }

    public Error(Integer state, String code, String message) {
        this.state = state;
        this.code = code;
        this.message = message;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
