package net.impacto.magazine.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Edition implements Serializable {
	
	private static final long serialVersionUID = 6091613428297590406L;

	private Integer id;
	private String lang;
	private String number;
	private Integer month;
	private Integer year;
	private String title;
	private String summary;
	private Boolean isPublished;
	private String publishedAt;
	private String createdAt;
	private FileLink mainImage;
	private FileLink coverpage;
	private List<FileLink> listConsolidated;
	private List<Content> listContent;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public FileLink getMainImage() {
		return mainImage;
	}

	public void setMainImage(FileLink mainImage) {
		this.mainImage = mainImage;
	}

	public FileLink getCoverpage() {
		return coverpage;
	}

	public void setCoverpage(FileLink coverpage) {
		this.coverpage = coverpage;
	}

	public List<FileLink> getListConsolidated() {
		return listConsolidated;
	}

	public void setListConsolidated(List<FileLink> listConsolidated) {
		this.listConsolidated = listConsolidated;
	}

	public List<Content> getListContent() {
		return listContent;
	}

	public void setListContent(List<Content> listContent) {
		this.listContent = listContent;
	}
}
