package net.impacto.magazine.dao;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.EditionFormatLanguageRequest;
import net.impacto.magazine.dtos.request.EditionImageHomeRequest;
import net.impacto.magazine.dtos.request.EditionLanguageRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersFileEditionRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Edition;
import net.impacto.magazine.entity.FileLink;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class EditionDAOTest {
	@Autowired
	private EditionDAO editionDAO;

	// @Test
	public void listFileEditionTest() {
		FiltersFileEditionRequest request = new FiltersFileEditionRequest();
		request.setEditionId(2);
		request.setLanguage("es");
		try {
		
		} catch (Exception e) {

		}
	}

	// @Test
	public void test() {
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		request.setPageNum(null);
		request.setIsLast(null);
		request.setPageSize(null);
		request.setEditionIds(null);
		request.setIdLanguage("es");
		request.setSortFilters("id");
		try {
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void insertEditionMainDataTest() {
		EditionRequest request = new EditionRequest();
		request.setUsername("jbecerra");
		Edition edi = new Edition();
		edi.setId(1);
		edi.setLang("fr");
		edi.setTitle("insertar main dao");
		edi.setSummary("insertar main dao summary");
		FileLink fileLink = new FileLink();
		fileLink.setName("imagen de prueba insertar main");
		fileLink.setLink("/ruta/ruta/imagen de insertar main");
		edi.setCoverpage(fileLink);
		request.setEdition(edi);
		request.setZoneId(3);
		
		try {	
				Confirmation response = new Confirmation();
				response = editionDAO.insertEditionMainData(request).get(0);
				System.out.println("RESPUESTA: " + response.getMessage() + response.getId() + response.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void insertEditionTest() {
		EditionRequest request = new EditionRequest();
		request.setUsername("jbecerra");
		Edition edi = new Edition();
		edi.setNumber("43");
		edi.setMonth(12);
		edi.setYear(2018);
		FileLink fileLink = new FileLink();
		fileLink.setName("imagen de prueba insertar");
		fileLink.setLink("/ruta/ruta/imagen de insertar");
		edi.setMainImage(fileLink);
		request.setEdition(edi);
		//request.setEdition(new Edition());
		try {	
				Confirmation response;
				response = editionDAO.insertEdition(request).get(0);
				System.out.println("RESPUESTA: " + response.getMessage() + response.getId() + response.getDataId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void updateEditionLanguageTest() {
		EditionLanguageRequest request = new EditionLanguageRequest();
		
		request.setEditionId(1);
		request.setLanguageCode("es");
		FileLink file = new FileLink();
		file.setLink("TOTAL español ACT LASTACT LASTACT LAST");
		file.setName("ESPAÑOL ACT LAST LAST LAST");
		request.setFile(file);
		
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void updateEditioFormatnLanguageTest() {
		EditionFormatLanguageRequest request = new EditionFormatLanguageRequest();
		request.setEditionId(1);
		request.setLanguageCode("es");
		request.setFileFormatExtension("pdf");
		request.setLinkConsolidated("/link de archivo actualizado/ archivo "); 

		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void updateEditionImageHomeTest() {
		EditionImageHomeRequest request = new EditionImageHomeRequest();
		request.setEditionId(2);
		FileLink file = new FileLink();
		file.setLink("/link de descarga/ actualizado','nombre de imagen");
		file.setName("nombre de imagen update");
		request.setFile(file);
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// @Test
	public void updateEditionFormatLanguageTest() {
		EditionRequest request = new EditionRequest();
		Edition edition = new Edition();


		request.setEdition(edition);
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void updateEditionTest() {
		EditionRequest request = new EditionRequest();
		Edition edition = new Edition();

		

		request.setEdition(edition);
		try {
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
