package net.impacto.magazine.dao;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.entity.Zone;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppDAOConfigTest.class})
public class ZoneDAOTest {
	
	@Autowired
	private ZoneDAO zoneDAO;

	@Test
	public void testListFileFormat() {
		try {
			FiltersZoneRequest request = new FiltersZoneRequest();
			request.setIstrue(true);
			List<Zone> listZones = zoneDAO.listZones(request);
			listZones.forEach( content -> {
				System.out.println( "FORMAT" +  content.getId() + "   " + content.getName() + content.getIsActive() );
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
