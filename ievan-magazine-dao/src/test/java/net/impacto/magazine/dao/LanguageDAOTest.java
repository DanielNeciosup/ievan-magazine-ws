package net.impacto.magazine.dao;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.entity.Language;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppDAOConfigTest.class})
public class LanguageDAOTest {
	
	@Autowired
	private LanguageDAO languageDAO;

	@Test
	public void testListLanguageZoneEdition() {
		try {
			FiltersLanguageRequest request = new FiltersLanguageRequest();
			request.setEditionId(1);
			request.setZoneId(1);
			
			List<Language> listLanguage = languageDAO.listLanguageZoneEdition(request); 
			listLanguage.forEach( content -> {
				System.out.println( "FORMAT" +  content.getId() + "   " + content.getName() + content.getCode() + content.getUsed() );
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
