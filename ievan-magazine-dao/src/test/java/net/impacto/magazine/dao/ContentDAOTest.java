package net.impacto.magazine.dao;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.entity.Content;
import net.impacto.magazine.entity.ContentEdition;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppDAOConfigTest.class})
public class ContentDAOTest {
	
	@Autowired
	private ContentDAO contentDAO;

	@Test
	public void test() {
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		request.setEditionIds("1");
		try {
			List<Content> listContents = contentDAO.listContents(request);
			listContents.forEach( content -> {
				//System.out.println( "EJEMPLO" +  content.getText() + "   " + content.getNumLocationPage() );
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//@Test
	public void ContentEditionTest() {
		ContentEdition req = new ContentEdition();
		//ContentEditionRequest request = new ContentEditionRequest();
		req.setIdContent(1);
		req.setIdEdition(1);
		req.setIdParent(null);
		req.setNumLocationPage(50);
		req.setShortDescription(null);
		req.setTxtTitle("Camino Title");
		//request.setContentEdition(req);
		try {
			//List<Confirmation> response = contentDAO.insertContentEdition(request);
			//response.forEach(content -> {System.out.println("PRUEBA DE INSERT "+ content.getId() + content.getMessage() + content.getDataId());});
		}catch(Exception e) {
			e.printStackTrace();
		}	
	}
}
