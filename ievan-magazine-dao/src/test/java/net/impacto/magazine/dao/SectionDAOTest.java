package net.impacto.magazine.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.entity.Section;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class SectionDAOTest {
	@Autowired
	private SectionDAO sectionDAO;
	
	@Test
	public void listSectionsTest() {
		FiltersSectionsRequest request = new FiltersSectionsRequest();
		try {
			List<Section> listSections = sectionDAO.listSections(request);
			listSections.forEach( section -> {
				System.out.println( section.getName() );
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}