package net.impacto.magazine.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.FileLink;
import net.impacto.magazine.entity.News;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class NewsDAOTest {
	@Autowired
	private NewsDAO newsDAO;
	
	@Test
	@Ignore
	public void createNewsTest()
	{
		NewsRequest request = new NewsRequest();
		
		News news = new News();
		FileLink mainImage = new FileLink();
		
		news.setTitle("COREA DEL NORTE: HUYENDO DE CRISTO, TERMINÓ SUFRIENDO POR AMOR A ÉL");
		news.setDescription("A causa  de los múltiples maltratos de su padre, Tracia, una adolescente de 12 años, planeaba su muerte. Aquel día, justo antes de que se quite la vida, recibió la invitación a un evento cristiano. Jamás imaginó que desde aquel instante, su historia cambiaría.");
		news.setContent("123 Con temperaturas superiores a los 30 grados, la alta humedad y la escasez de agua a causa de las torrenciales lluvias e inundaciones, Hiroshima, Ehime y Okayama, los lugares que actualmente se encuentran más afectados por el cambio climatológico, fueron declarados en estado de emergencia. Hasta el momento, se han reportado 200 víctimas mortales y varias decenas de personas continúan desaparecidas. “Más de 70.000 militares, policías y bomberos están trabajando entre los escombros en la búsqueda de los desaparecidos”, menciona Kofi, un militar en una entrevista para MHK. El desastre ha provocado la preocupación de todo un país. “Es innegable que este tipo de lluvias torrenciales y cambios inexplicables en el clima, se han ido incrementando en los últimos años\", manifestó el secretario del Gabinete, Yoshihide Suga, en una rueda de prensa.");
		news.setIsHighLighted(true);
		news.setCoverpagePosition(1);
		news.setPublishedAt("2018-07-16 18:10");
		
		mainImage.setName("Huye de Cristo en Corea 2018");
		mainImage.setLink("http://impactoevangelistico.net/imagenes/upload/julio2018/japon-portada-jeueves-20180712154431.jpg");
		
		news.setMainImage(mainImage);
		
		request.setNews(news);
		request.setTagsIds("5,6");
		request.setUsername("jbecerra");
		request.setIsSckeduled(true);
		
		try 
		{
			List<Confirmation> listConfirmations = newsDAO.createNews(request);
			listConfirmations.forEach(confirmation -> {
				System.out.println( String.format("%s::%s::%s", confirmation.getId(), confirmation.getMessage(), confirmation.getDataId() ) );
			});
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void hightlightNewsTest()
	{
		HighlightedNewsRequest request = new HighlightedNewsRequest();
		request.setNewsId(142);
		request.setLang("es");
		request.setCvpsPosition(1);
		
		try 
		{
			List<Confirmation> listConfirmations = newsDAO.hightlightNews(request);
			listConfirmations.forEach(confirmation -> {
				System.out.println( String.format("%s::%s::%s", confirmation.getId(), confirmation.getMessage(), confirmation.getDataId() ) );
			});
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
