package net.impacto.magazine.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.entity.Confirmation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class NoteDAOTest {
	@Autowired
	private NoteDAO noteDAO;
	
	@Test
	public void updateTagsTest()
	{
		String jsonString = "{\r\n" + 
							"	\"username\": null,\r\n" + 
							"	\"noteId\": 33,\r\n" + 
							"	\"listTags\": [{\r\n" + 
							"			\"id\": 3,\r\n" + 
							"			\"value\": \"Misiones\",\r\n" + 
							"			\"operationCode\": null\r\n" + 
							"		}, {\r\n" + 
							"			\"id\": 4,\r\n" + 
							"			\"value\": \"Paz\",\r\n" + 
							"			\"operationCode\": 1\r\n" + 
							"		}, {\r\n" + 
							"			\"id\": 5,\r\n" + 
							"			\"value\": null,\r\n" + 
							"			\"operationCode\": 3\r\n" + 
							"		}\r\n" + 
							"	]\r\n" + 
							"}";
		
		try 
		{
			List<Confirmation> listConfirmations = noteDAO.updateTags(jsonString);
			listConfirmations.forEach(c -> {
				System.out.println( String.format("id::%s message::%s", c.getId(), c.getMessage()) );
			});
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
