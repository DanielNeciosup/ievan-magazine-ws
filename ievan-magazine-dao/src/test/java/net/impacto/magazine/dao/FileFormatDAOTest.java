package net.impacto.magazine.dao;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.entity.FileFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppDAOConfigTest.class})
public class FileFormatDAOTest {
	
	@Autowired
	private FileFormatDAO fileFormatDAO;

	@Test
	public void testListFileFormat() {
		try {
			FiltersFileFormatRequest request = new FiltersFileFormatRequest();
			request.setIstrue(true);
			List<FileFormat> listFileFormats = fileFormatDAO.listFileFormat(request);
			listFileFormats.forEach( content -> {
				System.out.println( "FORMAT" +  content.getId() + "   " + content.getName() + content.getExtension() + content.getIsActive() );
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
