package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.entity.Item;

public interface TagDAO {

	public List<Item> listTags(FiltersTagsRequest request) throws Exception;
}
