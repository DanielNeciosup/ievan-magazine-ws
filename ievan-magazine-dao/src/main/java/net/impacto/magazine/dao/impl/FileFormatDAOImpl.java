package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.FileFormatDAO;
import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.entity.FileFormat;
import net.impacto.magazine.mapper.FileFormatMapper;

@Repository("fileFormatDAO")
@Transactional
public class FileFormatDAOImpl implements FileFormatDAO{
	
	@Autowired
	private FileFormatMapper fileFormatMapper;
	
	@Override
	public List<FileFormat> listFileFormat(FiltersFileFormatRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		params.put("active", request.getIstrue());
		return fileFormatMapper.listFileFormat(params);
	}
}
