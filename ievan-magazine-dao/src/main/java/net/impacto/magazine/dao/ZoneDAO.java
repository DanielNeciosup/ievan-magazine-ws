package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.entity.Zone;

public interface ZoneDAO {
	public List<Zone> listZones(FiltersZoneRequest request) throws Exception;
}
