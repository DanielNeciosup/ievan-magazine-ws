package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.entity.Section;

public interface SectionDAO {
	public List<Section> listSections(FiltersSectionsRequest request) throws Exception;
}
