package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.entity.Language;

public interface LanguageDAO {
	public List<Language> listLanguageZoneEdition(FiltersLanguageRequest request) throws Exception;
}
