package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FileLinkNewsRequest;
import net.impacto.magazine.dtos.request.FiltersNewsRequest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.News;

public interface NewsDAO {
	public List<News> listNews(FiltersNewsRequest request) throws Exception;

	public List<Confirmation> createNews(NewsRequest request) throws Exception;

	public List<Confirmation> updateNews(NewsRequest request) throws Exception;

	public List<Confirmation> updateImage(FileLinkNewsRequest request) throws Exception;

	public List<Confirmation> deleteNews(FiltersNewsRequest request) throws Exception;
	
	public List<Confirmation> hightlightNews(HighlightedNewsRequest request) throws Exception;
}
