package net.impacto.magazine.dao.impl;

import net.impacto.magazine.dao.NoteDAO;
import net.impacto.magazine.dtos.request.FiltersNotesRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Note;
import net.impacto.magazine.mapper.NoteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("noteDAO")
@Transactional
public class NoteDAOImpl implements NoteDAO {
    @Autowired
    private NoteMapper noteMapper;

    @Override
    public List<Note> listNotes(FiltersNotesRequest request) throws Exception {
        Map<String, Object> params = new HashMap<>();

        params.put("lang", request.getLang());
        params.put("noteId", request.getNoteId());
        params.put("sectionId", request.getSectionId());
        params.put("numPage", request.getPageNum());
        params.put("sizePage", request.getPageSize());
        params.put("updateViews", request.getUpdateViews());
        params.put("sort", request.getSort());
        params.put("flActive", request.getFlActive());
    	
        return noteMapper.listNotes(params);
    }

	@Override
	public List<Confirmation> updateTags(String jsonString) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("jsonString", jsonString);
		
		return noteMapper.updateTags(params);
	}
}
