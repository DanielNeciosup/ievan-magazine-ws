package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.CoverpageSectionDAO;
import net.impacto.magazine.dtos.request.FiltersCoverpageSectionRequest;
import net.impacto.magazine.entity.CoverpageSection;
import net.impacto.magazine.mapper.CoverpageSectionMapper;

@Repository("coverpageSectionDAO")
@Transactional
public class CoverpageSectionDAOImpl implements CoverpageSectionDAO{
	@Autowired
	private CoverpageSectionMapper cvpsMapper;	
	
	@Override
	public List<CoverpageSection> getCoverpageSection(FiltersCoverpageSectionRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("lang", request.getLang());
		params.put("position", request.getPosition());
		
		return cvpsMapper.getCoverpageSection(params);
	}
}
