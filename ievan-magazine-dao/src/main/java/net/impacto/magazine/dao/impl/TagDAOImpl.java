package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.impacto.magazine.dao.TagDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.entity.Item;
import net.impacto.magazine.mapper.TagMapper;

@Repository("tagDAO")
@Transactional
public class TagDAOImpl implements TagDAO{
	@Autowired
	private TagMapper tagMapper;
	
	@Override
	public List<Item> listTags(FiltersTagsRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		
		params.put("lang", request.getLang());
		params.put("tagId", request.getTagId());
		params.put("noteId", request.getNoteId());
		
		return tagMapper.listTags(params);
	}
}
