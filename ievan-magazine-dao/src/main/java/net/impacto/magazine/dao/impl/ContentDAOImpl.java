package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.ContentDAO;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Content;
import net.impacto.magazine.mapper.ContentMapper;

@Repository("contentDAO")
@Transactional
public class ContentDAOImpl implements ContentDAO {
	@Autowired
	private ContentMapper contentMapper;
	
	@Override
	public List<Content> listContents(FiltersEditionsRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		params.put("editionId", request.getEditionIds());
		return contentMapper.listContents(params);
	}
	
	@Override
	public List<Confirmation> insertContent(ContentRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		params.put("name", request.getContent().getName() );
		params.put("level", request.getContent().getLevel() );
		params.put("page", request.getContent().getPage() );
		params.put("description", request.getContent().getDescription() );
		params.put("root", request.getContent().getIsRoot() );
		params.put("imageName", request.getContent().getImage().getName() );
		params.put("imagePath", request.getContent().getImage().getLink() );
		return contentMapper.insertContent(params);
	}
}
