package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.NewsDAO;
import net.impacto.magazine.dtos.request.FileLinkNewsRequest;
import net.impacto.magazine.dtos.request.FiltersNewsRequest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.News;
import net.impacto.magazine.mapper.NewsMapper;

@Repository("newsDAO")
@Transactional
public class NewsDAOImpl implements NewsDAO{
	@Autowired
	private NewsMapper newsMapper;
	
	@Override
	public List<News> listNews(FiltersNewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("lang", request.getLang());
		params.put("id", request.getId());
		params.put("title", request.getTitle());
		params.put("description", request.getDescription());
		params.put("minViews", request.getMinViews());
		params.put("maxViews", request.getMaxViews());
		params.put("minPublishedAt", request.getMinPublishedAt());
		params.put("maxPublishedAt", request.getMaxPublishedAt());
		params.put("pageNum", request.getPageNum());
		params.put("pageSize", request.getPageSize());
		params.put("updateViews", request.getUpdateViews());
		params.put("sort", request.getSort());
		params.put("isPublished", request.getIsPublished());
		params.put("isHighlighted", request.getIsHighlighted());
		
		return newsMapper.listNews(params);
	}

	@Override
	public List<Confirmation> createNews(NewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		News news = request.getNews();
		
		params.put("lang", news.getLang());
		params.put("title", news.getTitle());
		params.put("description", news.getDescription());
		params.put("content", news.getContent());
		params.put("isHighlighted", news.getIsHighLighted());
		params.put("coverpagePosition", news.getCoverpagePosition());
		params.put("publishedAt", news.getPublishedAt());
		params.put("mainImageName", news.getMainImage().getName());
		params.put("mainImageLink", news.getMainImage().getLink());
		params.put("tagsIds", request.getTagsIds());
		params.put("userRegister", request.getUsername());
		params.put("isSckeduled", request.getIsSckeduled());
		
		return newsMapper.createNews(params);
	}

	@Override
	public List<Confirmation> updateNews(NewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		News news = request.getNews();
		
		params.put("lang", news.getLang());
		params.put("newsId", news.getId());
		params.put("title", news.getTitle());
		params.put("description", news.getDescription());
		params.put("content", news.getContent());
		
		return newsMapper.updateNews(params);
	}

	@Override
	public List<Confirmation> updateImage(FileLinkNewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("lang", request.getLang());
		params.put("newsId", request.getNewsId());
		params.put("mainImageName", request.getFile().getName());
		params.put("mainImageLink", request.getFile().getLink());
		
		return newsMapper.updateImage(params);
	}

	@Override
	public List<Confirmation> deleteNews(FiltersNewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("newsId", request.getId());
		
		return newsMapper.deleteNews(params);
	}

	@Override
	public List<Confirmation> hightlightNews(HighlightedNewsRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("newsId", request.getNewsId());
		params.put("lang", request.getLang());
		params.put("cvpsPosition", request.getCvpsPosition());
		
		return newsMapper.hightlightNews(params);
	}
}
