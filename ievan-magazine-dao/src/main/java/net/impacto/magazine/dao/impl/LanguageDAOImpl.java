package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.LanguageDAO;
import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.entity.Language;
import net.impacto.magazine.mapper.LanguageMapper;

@Repository("languageDAO")
@Transactional
public class LanguageDAOImpl implements LanguageDAO{
	
	@Autowired
	private LanguageMapper languageMapper;
	
	public List<Language> listLanguageZoneEdition(FiltersLanguageRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		params.put("idEdition", request.getEditionId());
		params.put("idZone", request.getZoneId());
		return languageMapper.listLanguageZoneEdition(params);
	}
}
