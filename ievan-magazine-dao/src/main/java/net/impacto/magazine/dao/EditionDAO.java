package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.entity.Confirmation;

public interface EditionDAO {

	List<Confirmation> insertEdition(EditionRequest request);
	List<Confirmation> insertEditionMainData(EditionRequest request);
	List<Confirmation> updateConsolidated(String objectToJSONString);
	List<Confirmation> insertContentEdition(ContentRequest request);
}
