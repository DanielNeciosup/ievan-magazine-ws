package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersCoverpageSectionRequest;
import net.impacto.magazine.entity.CoverpageSection;

public interface CoverpageSectionDAO {
	public List<CoverpageSection> getCoverpageSection(FiltersCoverpageSectionRequest request) throws Exception;
}
