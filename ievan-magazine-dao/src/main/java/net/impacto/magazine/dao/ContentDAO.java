package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Content;

public interface ContentDAO {
	public List<Content> listContents(FiltersEditionsRequest request) throws Exception;
	public List<Confirmation> insertContent(ContentRequest request) throws Exception;
	
}
