package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.impacto.magazine.entity.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.SectionDAO;
import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.mapper.SectionMapper;

@Repository("sectionDAO")
@Transactional
public class SectionDAOImpl implements SectionDAO {
	@Autowired
	private SectionMapper sectionMapper;

	@Override
	public List<Section> listSections(FiltersSectionsRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		
		params.put("lang", request.getLang());
		params.put("sectionId", request.getSectionId());
		
		return sectionMapper.listSections(params);
	}
}
