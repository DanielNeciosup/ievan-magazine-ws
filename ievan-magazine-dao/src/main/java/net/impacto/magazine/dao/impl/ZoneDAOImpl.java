package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import net.impacto.magazine.dao.ZoneDAO;
import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.entity.Zone;
import net.impacto.magazine.mapper.ZoneMapper;

@Repository("zoneDAO")
@Transactional
public class ZoneDAOImpl implements ZoneDAO {
	
	@Autowired
	private ZoneMapper zoneMapper;
	
	public List<Zone> listZones(FiltersZoneRequest request) throws Exception{
		Map<String, Object> params = new HashMap<>();
		params.put("active", request.getIstrue());
		return zoneMapper.listZones(params);
	}
}
