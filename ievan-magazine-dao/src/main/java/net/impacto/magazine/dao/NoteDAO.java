package net.impacto.magazine.dao;

import net.impacto.magazine.dtos.request.FiltersNotesRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Note;

import java.util.List;

public interface NoteDAO {
    public List<Note> listNotes(FiltersNotesRequest request) throws Exception;

    public List<Confirmation> updateTags(String jsonString) throws Exception;
}
