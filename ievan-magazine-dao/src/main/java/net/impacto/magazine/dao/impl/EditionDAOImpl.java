package net.impacto.magazine.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.magazine.dao.EditionDAO;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.mapper.EditionMapper;

@Repository("editionDAO")
@Transactional
public class EditionDAOImpl implements EditionDAO {
	@Autowired
	private EditionMapper editionMapper;

	@Override
	public List<Confirmation> insertEdition(EditionRequest request){
	    Map<String, Object> params = new HashMap<>();
	    
	    params.put("userRegister", request.getUsername());
	    params.put("imgPath", request.getEdition().getMainImage().getLink());
	    params.put("imgName", request.getEdition().getMainImage().getName());
	    params.put("numEdition", request.getEdition().getNumber());
        params.put("numMonth", request.getEdition().getMonth());
        params.put("numYears", request.getEdition().getYear());
        
        return editionMapper.insertEdition(params);
	}

	@Override
	public List<Confirmation> insertEditionMainData(EditionRequest request) {
		Map<String, Object> params = new HashMap<>();
	    
	    params.put("editionId", request.getEdition().getId());
	    params.put("zoneId", request.getZoneId());
	    params.put("langCode", request.getEdition().getLang());
	    params.put("title", request.getEdition().getTitle());
        params.put("summary", request.getEdition().getSummary());
        params.put("imgCoverpageLink", request.getEdition().getCoverpage().getLink());
        params.put("imgCoverpageName", request.getEdition().getCoverpage().getName());
	    
		return editionMapper.insertEditionMainData(params);
	}

	@Override
	public List<Confirmation> updateConsolidated(String objectToJSONString) {
		Map<String, Object> params = new HashMap<>();
	    
		params.put("jsonString", objectToJSONString);
		
		return editionMapper.updateConsolidated(params);
	}

	@Override
	public List<Confirmation> insertContentEdition(ContentRequest request) {
		Map<String, Object> params = new HashMap<>();
	    params.put("contentId", request.getEditionId());
	    params.put("lang", request.getLang());
	    params.put("zoneId", request.getZoneId());
	    params.put("parentId", request.getContent().getParentId());
	    params.put("name", request.getContent().getName());
	    params.put("level", request.getContent().getLevel());
	    params.put("page", request.getContent().getPage());
	    params.put("description", request.getContent().getDescription());
	    params.put("available", request.getContent().getIsAvailable());
	    params.put("active", request.getContent().getIsActive());
	    params.put("root", request.getContent().getIsRoot());
	    params.put("nameImage", request.getContent().getImage().getName());
	    params.put("linkImage", request.getContent().getImage().getLink());
	    
		return editionMapper.insertContentEdition(params);
	}
	
}
