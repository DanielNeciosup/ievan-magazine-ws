package net.impacto.magazine.dao;

import java.util.List;

import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.entity.FileFormat;

public interface FileFormatDAO {
	public List<FileFormat> listFileFormat(FiltersFileFormatRequest request) throws Exception;
}
