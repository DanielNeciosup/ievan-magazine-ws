package net.impacto.magazine.config;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
@MapperScan("net.impacto.magazine.mapper")
@PropertySource(value = "classpath:/net/impacto/magazine/config/log4j2.properties", ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/magazine/config/datasource.properties", ignoreResourceNotFound = true)
public class AppDAOConfig {
	@Value("${datasource.jndi-name}")
    private String jndiName;

	@Bean
    public DataSource getDataSourceByJNDI() {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        return lookup.getDataSource(jndiName);
    }
}