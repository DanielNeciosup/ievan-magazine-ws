package net.impacto.magazine.config;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {
	"net.impacto.magazine.dao",
	"net.impacto.magazine.mapper"
})  
@MapperScan("net.impacto.magazine.mapper")
@PropertySource(value = "classpath:/net/impacto/magazine/config/log4j2.properties", ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/magazine/config/jdbc.properties", ignoreResourceNotFound = true)
public class AppDAOConfigTest {
	@Value("${jdbc.username}")
	private String username;
	
	@Value("${jdbc.password}")
	private String password;
	
	@Value("${jdbc.url}")
	private String url;
	
	@Value("${jdbc.driverClassName}")
	private String driverClassName;
		
	@Bean
	public DataSource getDSIevanMgByCredentials() {
		return DataSourceBuilder.create()
			   .username(username)
			   .password(password)
			   .url(url)
			   .driverClassName(driverClassName)
			   .build();
	}
	
	@Bean
	public DataSourceTransactionManager transactionManager() throws Exception 
	{
		return new DataSourceTransactionManager(getDSIevanMgByCredentials());
	}

	@Bean
	public SqlSessionFactoryBean sqlSessionFactory() throws Exception 
	{
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();

		sessionFactory.setDataSource(getDSIevanMgByCredentials());

		return sessionFactory;
	}
}
