package net.impacto.magazine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanMagazineDaoApplication {
    public static void main(String[] args) {
    	SpringApplication.run(IevanMagazineDaoApplication.class, args);
    }
}
