package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Edition;
import net.impacto.magazine.entity.FileEdition;

@Mapper
public interface EditionMapper {
	
	public List<Confirmation> insertEdition(Map<String, Object> params);
	
	public List<Confirmation> insertEditionMainData(Map<String, Object> params);
	
	public List<Confirmation> insertContentEdition(Map<String, Object> params);
	
	// -----------------------------------------
	
	public List<Edition> listEditions(Map<String, Object> params);

	public List<Confirmation> updateEdition(Map<String, Object> params);

	public List<FileEdition> listFileEditions(Map<String, Object> params);

	public List<Confirmation> insertEditionFormatLanguage(Map<String, Object> params);

	public List<Confirmation> insertEditionLanguage(Map<String, Object> params);

	public List<Confirmation> updateEditionLanguage(Map<String, Object> params);

	public List<Confirmation> updateEditionFormatLanguage(Map<String, Object> params);
	
	public List<Confirmation> updateEditionImageHome(Map<String, Object> params);

	public List<Confirmation> updateConsolidated(Map<String, Object> params);
}
