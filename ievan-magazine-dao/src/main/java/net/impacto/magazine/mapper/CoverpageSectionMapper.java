package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import net.impacto.magazine.entity.CoverpageSection;

public interface CoverpageSectionMapper {
	public List<CoverpageSection> getCoverpageSection(Map<String, Object> params);
}
