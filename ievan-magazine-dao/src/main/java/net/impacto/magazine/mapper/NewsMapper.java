package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.News;

public interface NewsMapper {
	public List<News> listNews(Map<String, Object> params) throws Exception;

	public List<Confirmation> createNews(Map<String, Object> params) throws Exception;

	public List<Confirmation> updateNews(Map<String, Object> params);

	public List<Confirmation> updateImage(Map<String, Object> params);

	public List<Confirmation> deleteNews(Map<String, Object> params);
	
	public List<Confirmation> hightlightNews(Map<String, Object> params);
}
