package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import net.impacto.magazine.entity.Section;

@Mapper
public interface SectionMapper {
	public List<Section> listSections(Map<String, Object> params);
}
