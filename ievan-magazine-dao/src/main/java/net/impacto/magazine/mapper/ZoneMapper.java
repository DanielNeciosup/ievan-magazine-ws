package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import net.impacto.magazine.entity.Zone;

@Mapper
public interface ZoneMapper {
	public List<Zone> listZones(Map<String, Object> params);
}
