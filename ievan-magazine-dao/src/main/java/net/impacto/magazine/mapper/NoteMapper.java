package net.impacto.magazine.mapper;

import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Note;

import java.util.List;
import java.util.Map;

public interface NoteMapper {
    public List<Note> listNotes(Map<String, Object> params);

    public List<Confirmation> insertNote(Map<String, Object> params);
    
    public List<Confirmation> updateTags(Map<String, Object> params);
}
