package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import net.impacto.magazine.entity.FileFormat;

@Mapper
public interface FileFormatMapper {
	public List<FileFormat> listFileFormat(Map<String, Object> params);
}
