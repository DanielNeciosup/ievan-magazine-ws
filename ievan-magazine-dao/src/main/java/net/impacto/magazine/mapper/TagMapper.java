package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import net.impacto.magazine.entity.Item;

@Mapper
public interface TagMapper {
	public List<Item> listTags(Map<String, Object> params);
}
