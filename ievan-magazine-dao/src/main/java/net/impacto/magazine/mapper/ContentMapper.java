package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Content;

@Mapper
public interface ContentMapper {
	public List<Content> listContents(Map<String, Object> params);
	public List<Confirmation> insertContent(Map<String, Object> params);
}
