package net.impacto.magazine.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import net.impacto.magazine.entity.Language;

@Mapper
public interface LanguageMapper {
	public List<Language> listLanguageZoneEdition(Map<String, Object> params);
}
