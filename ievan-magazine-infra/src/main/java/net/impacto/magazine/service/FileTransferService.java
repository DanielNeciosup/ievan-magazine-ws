package net.impacto.magazine.service;

import net.impacto.magazine.entity.Credentials;

public interface FileTransferService {
	public String exportFileTo(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception;
	
	public String importFileFrom(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception;
}
