package net.impacto.magazine.service.impl;

import org.springframework.stereotype.Service;

import net.impacto.magazine.entity.Credentials;
import net.impacto.magazine.service.FileTransferService;
import net.impacto.magazine.utils.SCPUtil;

@Service("fileTransferSSHService")
public class FileTransferSSHServiceImpl implements FileTransferService{
	@Override
	public String exportFileTo(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception {
		return SCPUtil.exportFile(credentials, sourceFilePath, targetFolderPath);
	}

	@Override
	public String importFileFrom(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception {
		return SCPUtil.importFile(credentials, sourceFilePath, targetFolderPath);
	}
}
