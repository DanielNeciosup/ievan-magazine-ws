package net.impacto.magazine.service.impl;

import org.springframework.stereotype.Service;

import net.impacto.magazine.entity.Credentials;
import net.impacto.magazine.service.FileTransferService;

@Service("fileTransferFTPService")
public class FileTransferFTPServiceImpl implements FileTransferService{
	@Override
	public String exportFileTo(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception {
		return null;
	}

	@Override
	public String importFileFrom(Credentials credentials, String sourceFilePath, String targetFolderPath) throws Exception {
		return null;
	}
}
