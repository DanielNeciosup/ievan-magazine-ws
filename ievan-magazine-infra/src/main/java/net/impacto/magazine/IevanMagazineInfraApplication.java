package net.impacto.magazine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanMagazineInfraApplication {
	public static void main(String[] args) {
		SpringApplication.run(IevanMagazineInfraApplication.class, args);
	}
}
