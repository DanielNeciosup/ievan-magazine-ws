package net.impacto.magazine.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
	"net.impacto.magazine.service"
})
public class AppInfraConfig {

}
