package net.impacto.magazine.params;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("fileServerRoutesParams")
public class FileServerRoutesParams {
	
	@Value("${routes.multimedia}")
	private String multimediaPath;

	@Value("${routes.multimedia.editions.consolidated}")
	private String consolidatedEditionsPath;

	@Value("${routes.multimedia.editions.images}")
	private String imagesEditionsPath;

	@Value("${routes.multimedia.editions.videos}")
	private String videosEditionsPath;

	@Value("${routes.multimedia.editions.audios}")
	private String audiosEditionsPath;

	@Value("${routes.multimedia.news.images}")
	private String imagesNewsPath;

	@Value("${routes.multimedia.news.videos}")
	private String videosNewsPath;

	@Value("${routes.multimedia.news.audios}")
	private String audiosNewsPath;

	public String getMultimediaPath() {
		return multimediaPath;
	}

	public String getConsolidatedEditionsPath() {
		return consolidatedEditionsPath;
	}

	public String getImagesEditionsPath() {
		return imagesEditionsPath;
	}

	public String getVideosEditionsPath() {
		return videosEditionsPath;
	}

	public String getAudiosEditionsPath() {
		return audiosEditionsPath;
	}

	public String getImagesNewsPath() {
		return imagesNewsPath;
	}

	public String getVideosNewsPath() {
		return videosNewsPath;
	}

	public String getAudiosNewsPath() {
		return audiosNewsPath;
	}
}