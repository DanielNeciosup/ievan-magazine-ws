package net.impacto.magazine.params;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("webServicesRoutesParams")
public class WebServicesRoutesParams {
	@Value("${routes.notes.get}")
	private String listNotesRoute;

	@Value("${routes.editions.get}")
	private String listEditionsRoute;

	@Value("${routes.news.get}")
	private String listNewsRoute;

	public String getListNotesRoute() {
		return listNotesRoute;
	}

	public String getListEditionsRoute() {
		return listEditionsRoute;
	}

	public String getListNewsRoute() {
		return listNewsRoute;
	}
}
