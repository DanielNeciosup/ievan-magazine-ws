package net.impacto.magazine.params;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiParams {
    @Value("${api.name}")
    private String name;

    @Value("${api.description}")
    private String description;

    @Value("${api.version}")
    private String version;

    @Value("${api.author.name}")
    private String authorName;

    @Value("${api.author.email}")
    private String authorEmail;

    @Value("${api.author.web}")
    private String authorWeb;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorWeb() {
        return authorWeb;
    }

    public void setAuthorWeb(String authorWeb) {
        this.authorWeb = authorWeb;
    }
}
