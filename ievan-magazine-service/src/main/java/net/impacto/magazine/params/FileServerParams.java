package net.impacto.magazine.params;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("fileServerParams")
public class FileServerParams {
	@Value("${file-server.user}")
	private String user;

	@Value("${file-server.password}")
	private String password;

	@Value("${file-server.domain}")
	private String domain;

	@Value("${file-server.host}")
	private String host;

	@Value("${file-server.ssh-port}")
	private Integer sshPort;

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getDomain() {
		return domain;
	}

	public String getHost() {
		return host;
	}

	public Integer getSshPort() {
		return sshPort;
	}
}
