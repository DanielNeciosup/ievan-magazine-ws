package net.impacto.magazine.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {
	"net.impacto.magazine.service",
	"net.impacto.magazine.params"
})
@PropertySource(value = {"classpath:net/impacto/magazine/config/api.properties"}, ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/magazine/config/file-server.properties", ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/magazine/config/file-server-routes.properties", ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/magazine/config/web-services-routes.properties", ignoreResourceNotFound = true)
public class AppServiceConfig {

}
