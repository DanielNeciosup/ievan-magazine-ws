package net.impacto.magazine.helpers;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ResponseApiHelper {
    public static String getErrorCodeByDate( String apiName )
    {
        Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String errorCode = formatter.format(new Date());

        return String.format( "%s-%s", apiName, errorCode );
    }
}
