package net.impacto.magazine.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONHelper {
	public static <T> String objectToJSONString(T data) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		String stringData = mapper.writeValueAsString(data);

		return stringData;
	}

	public static <T> T stringToJSON( String jsonString, String fieldName, Class<T> clazz ) throws Exception
	{
		T object = (T) clazz.getDeclaredConstructor().newInstance();
		
		ObjectMapper mapper = new ObjectMapper();
	   
		JsonNode actualObj = mapper.readTree(jsonString);
		
		if( fieldName != null && !fieldName.equals( "" ) )
		{
			actualObj = actualObj.get(fieldName);
		}
		
		object = mapper.treeToValue(actualObj, clazz);
		
		return object;
	}
}
