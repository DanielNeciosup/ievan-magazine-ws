package net.impacto.magazine.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper
{
	/**
	 * yyyy-MM-dd HH:mm:sss.SSS
	 */
   public static final String DATE_FORMAT_1 = "yyyy-MM-dd HH:mm:sss.SSS";
   /**
    * dd/MM/yyyy HH:mm
    */
   public static final String DATE_FORMAT_2 = "dd/MM/yyyy HH:mm";
   /**
    * dd/MM/yyyy
    */
   public static final String DATE_FORMAT_3 = "dd/MM/yyyy";
   /**
    * "yyyy-mm-dd
    */
   public static final String DATE_FORMAT_4 = "yyyy-MM-dd";

   private static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:sss.SSS";

   public static String toString( String originalDate, String DATE_FORMAT_ORI, String DATE_FORMAT_DEST ) throws Exception
   {
       String targetDate = null;

       DateFormat originalFormat = new SimpleDateFormat(DATE_FORMAT_ORI);
       DateFormat destFormat = new SimpleDateFormat(DATE_FORMAT_DEST);

       Date date = originalFormat.parse(originalDate);
       targetDate = destFormat.format(date);

       return targetDate;
   }

   public static Date toDate( String dateString, String dateFormat ) throws Exception
   {
       DateFormat df = new SimpleDateFormat(dateFormat);
       return df.parse(dateString);
   }

   public static String toString( Date date, String dateFormat )
   {
       DateFormat df = new SimpleDateFormat(dateFormat);
       return df.format(date);
   }

   public static String currentDateToString(String dateFormat)
   {
       Date today = Calendar.getInstance().getTime();
       DateFormat df = new SimpleDateFormat(dateFormat);
       return df.format(today);
   }

   public static String currentDateToString()
   {
       return currentDateToString(DATE_FORMAT_DEFAULT);
   }
}	
