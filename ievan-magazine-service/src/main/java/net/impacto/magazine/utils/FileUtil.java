package net.impacto.magazine.utils;

import java.io.File;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

public class FileUtil {
	public static File rename(File file, String newName) throws Exception
	{
		File fileTemp = new File(newName);
		
		file.renameTo(fileTemp);
		
		return fileTemp;
	}
	
	public static void delete(String filePath) throws Exception {
		File file = new File(filePath);

		if (file.exists()) {
			file.delete();
		}
		
		file = null;
	}

	public static File decodeToFile(String fileBase64, String fileName) throws Exception {
		byte[] parseBase64Binary = Base64.decodeBase64(fileBase64);
		File file = new File(fileName);
		FileUtils.writeByteArrayToFile(file, parseBase64Binary);
		return file;
	}
}
