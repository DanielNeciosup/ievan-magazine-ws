package net.impacto.magazine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.FileFormatDAO;
import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListFileFormatsResponse;
import net.impacto.magazine.service.FileFormatService;
import net.impacto.magazine.service.ResponseService;

@Service("fileFormatService")
public class FileFormatServiceImpl implements FileFormatService{
	
	@Autowired
	private ResponseService responseService; 
	
	@Autowired
	private FileFormatDAO fileFormatDAO;

	@Override
	public BaseResponse<ListFileFormatsResponse> listFileFormat(FiltersFileFormatRequest request) throws Exception{
		BaseResponse<ListFileFormatsResponse> response = new BaseResponse<>();
		ListFileFormatsResponse data = new ListFileFormatsResponse();
		data.setListFileFormat(fileFormatDAO.listFileFormat(request));
		return responseService.onSuccess(response, data, null);
	}
}
