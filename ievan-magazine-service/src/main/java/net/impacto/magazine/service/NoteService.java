package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersNotesRequest;
import net.impacto.magazine.dtos.request.ListTagsNoteRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNotesResponse;
import net.impacto.magazine.dtos.response.NoteResponse;

public interface NoteService {
    public BaseResponse<ListNotesResponse> listNotes(FiltersNotesRequest request) throws Exception;

    public BaseResponse<NoteResponse> getNote(FiltersNotesRequest request) throws Exception;

    public BaseResponse<?> updateTags(ListTagsNoteRequest request) throws Exception;
}
