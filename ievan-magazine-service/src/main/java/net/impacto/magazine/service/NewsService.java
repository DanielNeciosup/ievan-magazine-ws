package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FileBase64NewsRequest;
import net.impacto.magazine.dtos.request.FiltersNewsRequest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNewsResponse;
import net.impacto.magazine.dtos.response.NewsResponse;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;

public interface NewsService {
	public BaseResponse<ListNewsResponse> listNews(FiltersNewsRequest request) throws Exception;
	
	public BaseResponse<NewsResponse> getNews(FiltersNewsRequest request) throws Exception;
	
	public BaseResponse<NewsResponse> createNews(NewsRequest request) throws Exception;
	
	public BaseResponse<NewsResponse> updateNews(NewsRequest request) throws Exception;
	
	public BaseResponse<FileLink> updateImage(FileBase64NewsRequest request) throws Exception;

	public BaseResponse<?> deleteNews(FiltersNewsRequest request) throws Exception;
	
	public BaseResponse<FileLink> transferMainImgNews(FileBase64 request) throws Exception;
	
	public BaseResponse<?> hightlightNews(HighlightedNewsRequest request) throws Exception;
}
