package net.impacto.magazine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.CoverpageSectionDAO;
import net.impacto.magazine.dtos.request.FiltersCoverpageSectionRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.CoverpageSectionResponse;
import net.impacto.magazine.entity.CoverpageSection;
import net.impacto.magazine.params.FileServerParams;
import net.impacto.magazine.service.CoverpageSectionService;
import net.impacto.magazine.service.ResponseService;

@Service("coverpageSectionService")
public class CoverpageSectionServiceImpl implements CoverpageSectionService{
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private CoverpageSectionDAO cvpSectionDAO;
	
	@Autowired
	private FileServerParams fsParams;
	
	@Override
	public BaseResponse<CoverpageSectionResponse> getCoverpageSection(FiltersCoverpageSectionRequest request) throws Exception {
		BaseResponse<CoverpageSectionResponse> response = new BaseResponse<>();
		
		CoverpageSectionResponse data = new CoverpageSectionResponse();
		
		CoverpageSection cvpSection = cvpSectionDAO.getCoverpageSection(request).get(0);
		
		if (cvpSection.getImage().getLink() != null) {
			cvpSection.getImage().setLink(String.format("%s%s", fsParams.getDomain(), cvpSection.getImage().getLink()));
		}
		
		data.setCvpSection(cvpSection);
		
		response = responseService.onSuccess(response, data, null);
		
		return response;
	}
}
