package net.impacto.magazine.service;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersFileEditionRequest;
import net.impacto.magazine.dtos.request.ListConsolidatedRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.EditionResponse;
import net.impacto.magazine.dtos.response.ListEditionContentsResponse;
import net.impacto.magazine.dtos.response.ListEditionsResponse;
import net.impacto.magazine.dtos.response.ListFileEditionResponse;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;

public interface EditionService {
	public BaseResponse<EditionResponse> insertEdition(EditionRequest request) throws Exception;
	public BaseResponse<EditionResponse> registerMainEdition(EditionRequest request) throws Exception;
	public BaseResponse<?> updateConsolidatedEdition(ListConsolidatedRequest request) throws Exception;
	public BaseResponse<FileLink> transferFileToServer(String fileBase64, String fileName, String codeLanguage) throws Exception;
	public BaseResponse<?> insertContentEdition(ContentRequest request) throws Exception;
	
	
	public BaseResponse<FileLink> transferImageToServer(FileBase64 request) throws Exception;
	public BaseResponse<FileLink> updateImageHome(FileBase64 request) throws Exception;
	//public BaseResponse<FileLink> sendEditionImgToFileServer(EditionRequest request) throws Exception;
	//public BaseResponse<FileLink> sendEditionCoverpageToFileServer(EditionRequest request) throws Exception;
	public BaseResponse<EditionResponse> updateEditions(EditionRequest request) throws Exception;
	public BaseResponse<ListEditionsResponse> listEditions(FiltersEditionsRequest request) throws Exception;
	public BaseResponse<ListEditionContentsResponse> listEditionContents(FiltersEditionsRequest request) throws Exception;
	public BaseResponse<ListEditionContentsResponse> listLastEditionContents() throws Exception;
	public BaseResponse<ListFileEditionResponse> listFilesEditions(FiltersFileEditionRequest request) throws Exception;
}