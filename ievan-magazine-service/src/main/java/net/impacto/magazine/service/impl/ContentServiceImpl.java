package net.impacto.magazine.service.impl;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.ContentDAO;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;
import net.impacto.magazine.params.FileServerRoutesParams;
import net.impacto.magazine.service.ContentService;
import net.impacto.magazine.service.FileServerService;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.utils.FileUtil;

@Service("contentService")
public class ContentServiceImpl implements ContentService {
	@Autowired
	private ContentDAO contentDAO;
	
	@Autowired
	private ResponseService responseService; 
	
	@Autowired
	private FileServerRoutesParams fsRoutesParams;
	
	@Autowired
	private FileServerService fileServerService;
	
	@Override
	public BaseResponse<?> insertContent(ContentRequest request) throws Exception{
		BaseResponse<?> response = new BaseResponse<>();
		List<Confirmation> listConfirmatiion = contentDAO.insertContent(request);
		request.getContent().setImage(transferImg(request.getImage()).getData());
		response = responseService.onSuccess(response, null, null, listConfirmatiion.get(0).getMessage());
		return response;
	}
	
	@Override
	public BaseResponse<FileLink> transferImg(FileBase64 request) throws Exception {
		BaseResponse<FileLink> response = new BaseResponse<>();
		
		File image = FileUtil.decodeToFile(request.getEncodeBase64(), request.getName());
		
		String originalName = image.getName();
		String newName = UUID.randomUUID().toString();
		
		image = FileUtil.rename(image, newName + "." + FilenameUtils.getExtension(image.getName()));
		
		String targetFolder = String.format("%s/%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getImagesEditionsPath());
		
		/**
		 * Export image to file server by SSH
		 */
		fileServerService.exportTo(image.getPath(), targetFolder);
		
		FileLink mainImage = new FileLink();
		mainImage.setLink(String.format("%s/%s", fsRoutesParams.getImagesEditionsPath(), image.getName()));
		mainImage.setName(FilenameUtils.removeExtension(originalName));
		
		FileUtil.delete(image.getPath());
		
		return responseService.onSuccess(response, mainImage, null);
	}
}
