package net.impacto.magazine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.LanguageDAO;
import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListLanguagesResponse;
import net.impacto.magazine.service.LanguageService;
import net.impacto.magazine.service.ResponseService;

@Service("languageService")
public class LanguageServiceImpl  implements LanguageService {
	
	@Autowired
	private ResponseService responseService; 
	
	@Autowired
	private LanguageDAO languageDAO;
	
	public BaseResponse<ListLanguagesResponse> listLanguages(FiltersLanguageRequest request) throws Exception{
		BaseResponse<ListLanguagesResponse> response = new BaseResponse<>();
		ListLanguagesResponse data = new ListLanguagesResponse();
		data.setListLanguage(languageDAO.listLanguageZoneEdition(request));
		return responseService.onSuccess(response, data, null);
	}
}
