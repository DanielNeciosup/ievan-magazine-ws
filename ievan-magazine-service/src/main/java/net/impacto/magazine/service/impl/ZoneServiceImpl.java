package net.impacto.magazine.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.ZoneDAO;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListZonesResponse;
import net.impacto.magazine.entity.Zone;
import net.impacto.magazine.entity.Language;
import net.impacto.magazine.service.LanguageService;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.ZoneService;

@Service("zoneService")
public class ZoneServiceImpl implements ZoneService {

	@Autowired
	private ResponseService responseService;

	@Autowired
	private ZoneDAO zoneDAO;

	@Autowired
	private LanguageService languageService;

	@Override
	public BaseResponse<ListZonesResponse> listZones(FiltersZoneRequest request) throws Exception {
		BaseResponse<ListZonesResponse> response = new BaseResponse<>();
		ListZonesResponse data = new ListZonesResponse();
		data.setListZone(zoneDAO.listZones(request));
		return responseService.onSuccess(response, data, null);
	}

	@Override
	public BaseResponse<ListZonesResponse> listLanguageZonesEditions(FiltersEditionsRequest request) throws Exception {
		BaseResponse<ListZonesResponse> response = new BaseResponse<>();
		ListZonesResponse data = new ListZonesResponse();
		FiltersZoneRequest req = new FiltersZoneRequest();
		List<Zone> listZone = new ArrayList<>();
		listZone = listZones(req).getData().getListZone();
		for (Zone zone : listZone) {
			FiltersLanguageRequest fil = new FiltersLanguageRequest();
			fil.setEditionId(Integer.parseInt(request.getEditionIds()));
			fil.setZoneId(zone.getId());

			List<Language> listLanguage ;
			listLanguage = languageService.listLanguages(fil).getData().getListLanguage();
			zone.setListLanguageNotUsed(new ArrayList<>());
			zone.setListLanguagesUsed(new ArrayList<>());
			for (Language lang : listLanguage) {
				if(lang.getUsed() == true) {
					zone.getListLanguagesUsed().add(lang);
				}else {
					zone.getListLanguageNotUsed().add(lang);
				}
			}
		}
		data.setListZone(listZone);
		return responseService.onSuccess(response, data, null);
	}
}
