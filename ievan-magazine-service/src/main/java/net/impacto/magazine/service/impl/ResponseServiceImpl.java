package net.impacto.magazine.service.impl;

import net.impacto.magazine.dtos.request.FiltersPaginationRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.entity.Error;
import net.impacto.magazine.entity.Pagination;
import net.impacto.magazine.enums.ResponseApi;
import net.impacto.magazine.helpers.ResponseApiHelper;
import net.impacto.magazine.params.ApiParams;
import net.impacto.magazine.service.ResponseService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ResponseServiceImpl implements ResponseService {
    private transient final Logger LOG = LogManager.getLogger(this.getClass());

    @Autowired
    private ApiParams apiParams;

    @Override
    public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination, String message) {
        response.setState(ResponseApi.OK.getCode());
        response.setData(data);
        response.setMessage(message);
        response.setPagination(pagination);
        
        return response;
    }

    @Override
    public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination) {
        return onSuccess(response, data, pagination, ResponseApi.OK.getMessage());
    }

    @Override
    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response, String message) {
        String errorCode = ResponseApiHelper.getErrorCodeByDate(apiParams.getName());
        response.setState(ResponseApi.SERVER_ERROR.getCode());
        response.setMessage(ResponseApi.SERVER_ERROR.getMessage());
        response.addError(new Error(ResponseApi.SERVER_ERROR.getCode(), errorCode, message));
        LOG.error(errorCode, e);
        
        return response;
    }

    @Override
    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response) {
        return onServerError(e, response, ResponseApi.SERVER_ERROR.getMessage());
    }

    @Override
    public <T> BaseResponse<T> addBusinessError(BaseResponse<T> response, String message) {
        response.setState(ResponseApi.BUSINESS_ERROR.getCode());
        response.setMessage(ResponseApi.BUSINESS_ERROR.getMessage());
        response.addError(new Error(ResponseApi.BUSINESS_ERROR.getCode(), message));
        
        return response;
    }

	@Override
	public <T> Pagination calculatePagination(boolean isEmptyList, FiltersPaginationRequest fpgRequest, Integer total, String uriWebService) {
		Pagination pagination = null;

		Integer pageNum = fpgRequest.getPageNum();
		Integer pageSize = fpgRequest.getPageSize();
	
		if (!isEmptyList && pageNum != null && pageSize != null) {
			pagination = new Pagination();
			
			pagination.setTotalElements(total);
			pagination.setCurrentPage(pageNum);
			Double totalPages = Math.ceil((double) (total / ((double) pageSize)));
			pagination.setTotalPages(totalPages.intValue());
			
			if (totalPages > pageNum) {
				pagination.setNextLink(String.format("%s?pageNum=%s&pageSize=%s", uriWebService, pageNum + 1, pageSize));
			}
			
			if (pageNum > 1 && pageNum <= totalPages){
				pagination.setBeforeLink(String.format("%s?pageNum=%s&pageSize=%s", uriWebService, pageNum - 1, pageSize));
			}			
		}
		
		return pagination;
	}
}
