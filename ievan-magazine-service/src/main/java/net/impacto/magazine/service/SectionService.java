package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListSectionsResponse;
import net.impacto.magazine.dtos.response.SectionResponse;

public interface SectionService {
	public BaseResponse<ListSectionsResponse> listSections(FiltersSectionsRequest request) throws  Exception;
	
	public BaseResponse<SectionResponse> getSection(FiltersSectionsRequest request) throws Exception;
}
