package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersLanguageRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListLanguagesResponse;

public interface LanguageService {
	public BaseResponse<ListLanguagesResponse> listLanguages(FiltersLanguageRequest request) throws Exception;
}
