package net.impacto.magazine.service.impl;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;

import net.impacto.magazine.dao.NewsDAO;
import net.impacto.magazine.dtos.request.FileBase64NewsRequest;
import net.impacto.magazine.dtos.request.FileLinkNewsRequest;
import net.impacto.magazine.dtos.request.FiltersNewsRequest;
import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.dtos.request.HighlightedNewsRequest;
import net.impacto.magazine.dtos.request.NewsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNewsResponse;
import net.impacto.magazine.dtos.response.NewsResponse;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;
import net.impacto.magazine.entity.Item;
import net.impacto.magazine.entity.News;
import net.impacto.magazine.entity.Pagination;
import net.impacto.magazine.enums.ResponseApi;
import net.impacto.magazine.params.FileServerParams;
import net.impacto.magazine.params.FileServerRoutesParams;
import net.impacto.magazine.params.WebServicesRoutesParams;
import net.impacto.magazine.service.FileServerService;
import net.impacto.magazine.service.NewsService;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.TagService;
import net.impacto.magazine.utils.FileUtil;

@Service("newsService")
public class NewsServiceImpl implements NewsService {
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private NewsDAO newsDAO;
	
	@Autowired
	private WebServicesRoutesParams wsRoutesParams;
	
	@Autowired
	private FileServerParams fsParams;
	
	@Autowired
	private FileServerRoutesParams fsRoutesParams;
	
	@Autowired
	private FileServerService fileServerService;
	
	@Autowired
	private TagService tagService;
	
	@Override
	public BaseResponse<ListNewsResponse> listNews(FiltersNewsRequest request) throws Exception {
		BaseResponse<ListNewsResponse> response = new BaseResponse<>();
		
		ListNewsResponse data = new ListNewsResponse();
		
		Pagination pagination = null;
		
		List<News> listNews = newsDAO.listNews(request);
		listNews.forEach(news -> {
			if (news.getMainImage() != null) {
				if (news.getMainImage().getLink() != null) {
					news.getMainImage().setLink(String.format("%s%s", fsParams.getDomain(), news.getMainImage().getLink()));
				}
			}
		});
		
		data.setListNews(listNews);
		
		if (!listNews.isEmpty()) {
			pagination = responseService.calculatePagination(listNews.isEmpty(), request, listNews.get(0).getTotal(), wsRoutesParams.getListNewsRoute());
		}
		
		return responseService.onSuccess(response, data, pagination);
	}

	@Override
	public BaseResponse<NewsResponse> getNews(FiltersNewsRequest request) throws Exception {
		BaseResponse<NewsResponse> response = new BaseResponse<>();
		
		NewsResponse data = new NewsResponse();
		
		List<News> listNews = listNews(request).getData().getListNews();
		News news = listNews != null && !listNews.isEmpty() ? listNews.get(0) : null;
		
		if (news == null) {
			responseService.addBusinessError(response, "Noticia no encontrada");
		}
		else
		{
			FiltersTagsRequest ftRq = new FiltersTagsRequest();
			ftRq.setNoteId(news.getId());
			ftRq.setLang(request.getLang());
			news.setListTags(tagService.listTags(ftRq).getData().getListTag());
			data.setNews(news);
			
			responseService.onSuccess(response, data, null);
		}
		
		return response;
	}

	@Override
	public BaseResponse<NewsResponse> createNews(NewsRequest request) throws Exception {
		BaseResponse<NewsResponse> response = new BaseResponse<>();
		
		NewsResponse data = new NewsResponse();
		
		/**
		 * Export main image of news in file server
		 */
		if (request.getImgBase64() != null) {			
			request.getNews().setMainImage(transferMainImgNews(request.getImgBase64()).getData());
		}
		
		/**
		 * Concatenate tag identifiers in string separate with commas
		 */
		if (request.getNews().getListTags() != null && !request.getNews().getListTags().isEmpty()) {
			List<Integer> listIdsTags = request.getNews().getListTags().stream().map(Item::getId).collect(Collectors.toList());
			String tagsIds = Joiner.on(",").join(listIdsTags);
			request.setTagsIds(tagsIds);
		}
		
		List<Confirmation> listConfirmations = newsDAO.createNews(request);
		Confirmation confirmation = listConfirmations != null && !listConfirmations.isEmpty() ? listConfirmations.get(0) : null;
		
		/*
		 * Read the results after of the insertion
		 */
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				FiltersNewsRequest fnRq = new FiltersNewsRequest();
				fnRq.setId(confirmation.getDataId().toString());
				fnRq.setLang(request.getLang());
				data.setNews(getNews(fnRq).getData().getNews());
				responseService.onSuccess(response, data, null);
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				listConfirmations.forEach(c -> {
					responseService.addBusinessError(response, c.getMessage());
				});
			}
		}
		
		return response;
	}

	@Override
	public BaseResponse<NewsResponse> updateNews(NewsRequest request) throws Exception {
		BaseResponse<NewsResponse> response = new BaseResponse<>();
		
		NewsResponse data = new NewsResponse();
		
		List<Confirmation> listConfirmations = newsDAO.updateNews(request);
		Confirmation confirmation = listConfirmations.isEmpty() ? null : listConfirmations.get(0);
		
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				FiltersNewsRequest fnRq = new FiltersNewsRequest();
				fnRq.setId(confirmation.getDataId().toString());
				fnRq.setLang(request.getLang());
				data.setNews(getNews(fnRq).getData().getNews());
				responseService.onSuccess(response, data, null);
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				listConfirmations.forEach(c -> {
					responseService.addBusinessError(response, c.getMessage());
				});
			}
		}
		
		return response;
	}

	@Override
	public BaseResponse<FileLink> updateImage(FileBase64NewsRequest request) throws Exception {
		BaseResponse<FileLink> response = new BaseResponse<>();
		
		response = transferMainImgNews(request.getFile());
		FileLink fileLink = response.getData();
		
		Preconditions.checkNotNull(fileLink);
		
		FileLinkNewsRequest flNewsRq = new FileLinkNewsRequest();
		flNewsRq.setNewsId(request.getNewsId());
		flNewsRq.setFile(fileLink);
		flNewsRq.setLang(request.getLang());
		List<Confirmation> listConfirmations = newsDAO.updateImage(flNewsRq);
		Confirmation confirmation = listConfirmations.isEmpty() ? null : listConfirmations.get(0);
		
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				fileLink.setLink(String.format("%s%s", fsParams.getDomain(), fileLink.getLink()));
				responseService.onSuccess(response, fileLink, null);
			}
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				for (Confirmation c : listConfirmations) {
					responseService.addBusinessError(response, c.getMessage());
				}
			}
		}
		
		return response;
	}

	@Override
	public BaseResponse<?> deleteNews(FiltersNewsRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		
	    List<Confirmation> listConfirmations = newsDAO.deleteNews(request);
	    Confirmation confirmation = listConfirmations.isEmpty() ? null : listConfirmations.get(0);
	    
	    if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				responseService.onSuccess(response, null, null, confirmation.getMessage());
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				for (Confirmation c : listConfirmations) {
					responseService.addBusinessError(response, c.getMessage());
				}
			}
		}
		
		return response;
	}

	@Override
	public BaseResponse<FileLink> transferMainImgNews(FileBase64 request) throws Exception {
		BaseResponse<FileLink> response = new BaseResponse<>();
		
		File image = FileUtil.decodeToFile(request.getEncodeBase64(), request.getName());
		
		String originalName = image.getName();
		String newName = UUID.randomUUID().toString();
		
		image = FileUtil.rename(image, newName + "." + FilenameUtils.getExtension(image.getName()));
		
		String targetFolder = String.format("%s/%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getImagesNewsPath());
		
		/**
		 * Export image to file server by SSH
		 */
		fileServerService.exportTo(image.getPath(), targetFolder);
		
		FileLink mainImage = new FileLink();
		mainImage.setLink(String.format("%s/%s", fsRoutesParams.getImagesNewsPath(), image.getName()));
		mainImage.setName(FilenameUtils.removeExtension(originalName));
		
		FileUtil.delete(image.getPath());
		
		return responseService.onSuccess(response, mainImage, null);
	}

	@Override
	public BaseResponse<?> hightlightNews(HighlightedNewsRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		
		List<Confirmation> listConfirmations = newsDAO.hightlightNews(request);
		Confirmation confirmation = listConfirmations.isEmpty() ? null : listConfirmations.get(0);
	    
	    if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				responseService.onSuccess(response, null, null, confirmation.getMessage());
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				for (Confirmation c : listConfirmations) {
					responseService.addBusinessError(response, c.getMessage());
				}
			}
		}
		
		return response;
	}
}
