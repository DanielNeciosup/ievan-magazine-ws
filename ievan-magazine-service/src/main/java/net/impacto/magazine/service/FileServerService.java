package net.impacto.magazine.service;

import net.impacto.magazine.dtos.response.BaseResponse;

public interface FileServerService {
	public BaseResponse<String> exportTo(String sourceFilePath, String targetFolderPath) throws Exception;
	
	public BaseResponse<String> importFrom(String sourceFilePath, String targetFolderPath) throws Exception;
}
