package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersFileFormatRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListFileFormatsResponse;

public interface FileFormatService {
	public BaseResponse<ListFileFormatsResponse> listFileFormat(FiltersFileFormatRequest request) throws Exception;
}
