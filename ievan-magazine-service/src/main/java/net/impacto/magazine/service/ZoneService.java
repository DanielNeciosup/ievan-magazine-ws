package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersZoneRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListZonesResponse;

public interface ZoneService {
	public BaseResponse<ListZonesResponse> listZones(FiltersZoneRequest request) throws Exception;
	public BaseResponse<ListZonesResponse> listLanguageZonesEditions(FiltersEditionsRequest request) throws Exception;
}
