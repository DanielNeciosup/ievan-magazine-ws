package net.impacto.magazine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.entity.Credentials;
import net.impacto.magazine.params.FileServerParams;
import net.impacto.magazine.service.AccessService;

@Service("accessFileServerService")
public class AccessFileServerService implements AccessService{
	@Autowired
	private FileServerParams fileServerParams;
	
	@Override
	public Credentials getCredentials() throws Exception {
		Credentials credentials = new Credentials();
		credentials.setUser(fileServerParams.getUser());
		credentials.setPassword(fileServerParams.getPassword());
		credentials.setHost(fileServerParams.getHost());
		credentials.setSshPort(fileServerParams.getSshPort());
		
		return credentials;
	}
}
