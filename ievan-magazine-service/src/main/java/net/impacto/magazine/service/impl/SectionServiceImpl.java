package net.impacto.magazine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.SectionDAO;
import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListSectionsResponse;
import net.impacto.magazine.dtos.response.SectionResponse;
import net.impacto.magazine.entity.Section;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.SectionService;

@Service("sectionService")
public class SectionServiceImpl implements SectionService {
	@Autowired
	private ResponseService responseService;
	
    @Autowired
    private SectionDAO sectionDAO;

    @Override
    public BaseResponse<ListSectionsResponse> listSections(FiltersSectionsRequest request) throws Exception {
    	BaseResponse<ListSectionsResponse> response = new BaseResponse<>();
    	
        ListSectionsResponse data = new ListSectionsResponse();

        var listSections = sectionDAO.listSections(request);
        data.setListSections(listSections);
        
        responseService.onSuccess(response, data, null);

        return response;
    }

	@Override
	public BaseResponse<SectionResponse> getSection(FiltersSectionsRequest request) throws Exception {
		BaseResponse<SectionResponse> response = new BaseResponse<>();
		
		SectionResponse data = new SectionResponse();
		
		Section section = listSections(request).getData().getListSections().get(0);
		data.setSection(section);
		
		if (section != null) {
			responseService.onSuccess(response, data, null);			
		}
		else{
			responseService.addBusinessError(response, "No encontrado");
		}
		
		return response;
	}
}
