package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListTagResponse;

public interface TagService {
	public BaseResponse<ListTagResponse> listTags(FiltersTagsRequest request) throws Exception;
}
