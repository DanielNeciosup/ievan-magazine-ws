package net.impacto.magazine.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.NoteDAO;
import net.impacto.magazine.dtos.request.FiltersNotesRequest;
import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.dtos.request.ListTagsNoteRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListNotesResponse;
import net.impacto.magazine.dtos.response.NoteResponse;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Note;
import net.impacto.magazine.entity.Pagination;
import net.impacto.magazine.enums.ResponseApi;
import net.impacto.magazine.helpers.JSONHelper;
import net.impacto.magazine.params.FileServerParams;
import net.impacto.magazine.params.WebServicesRoutesParams;
import net.impacto.magazine.service.NoteService;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.TagService;

@Service("noteService")
public class NoteServiceImpl implements NoteService {
	@Autowired
	private ResponseService responseService;

	@Autowired
	private NoteDAO noteDAO;

	@Autowired
	private TagService tagService;
	
	@Autowired
	private WebServicesRoutesParams wsRoutesParams;
	
	@Autowired
	private FileServerParams fsParams;

	@Override
	public BaseResponse<ListNotesResponse> listNotes(FiltersNotesRequest request) throws Exception {
		BaseResponse<ListNotesResponse> response = new BaseResponse<>();
		Pagination pagination = null;
		
		ListNotesResponse data = new ListNotesResponse();

		List<Note> listNotes = noteDAO.listNotes(request);
		listNotes.forEach(note -> {
			if (note.getMainImage() != null) {
				if (note.getMainImage().getLink() != null) {
					note.getMainImage().setLink(String.format("%s%s", fsParams.getDomain(), note.getMainImage().getLink()));
				}
			}
		});
		
		data.setListNotes(listNotes);
		
		if (!listNotes.isEmpty()) {
			pagination = responseService.calculatePagination(listNotes.isEmpty(), request, listNotes.get(0).getTotal(), wsRoutesParams.getListNotesRoute());
		}
		
		return responseService.onSuccess(response, data, pagination);
	}

	@Override
	public BaseResponse<NoteResponse> getNote(FiltersNotesRequest request) throws Exception {
		BaseResponse<NoteResponse> response = new BaseResponse<>();
		FiltersTagsRequest ftRq = new FiltersTagsRequest();
		NoteResponse data = new NoteResponse();
		
		ListNotesResponse lnRp = listNotes(request).getData();

		if (lnRp.getListNotes() != null && !lnRp.getListNotes().isEmpty()) {

			Note note = lnRp.getListNotes().get(0);
			ftRq.setNoteId(note.getId());
			ftRq.setLang(request.getLang());
			note.setListTags(tagService.listTags(ftRq).getData().getListTag());
			data.setNote(note);
			
			responseService.onSuccess(response, data, null);
		}
		else {
			responseService.addBusinessError(response, "Nota no encontrada");
		}

		return response;
	}

	@Override
	public BaseResponse<?> updateTags(ListTagsNoteRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		
		List<Confirmation> listConfirmations = noteDAO.updateTags(JSONHelper.objectToJSONString(request));
		Confirmation confirmation = listConfirmations != null && !listConfirmations.isEmpty() ? listConfirmations.get(0) : null;
		
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				responseService.onSuccess(response, null, null);
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				listConfirmations.forEach(c -> {
					responseService.addBusinessError(response, c.getMessage());
				});
			}
		}
		
		return response;
	}
}
