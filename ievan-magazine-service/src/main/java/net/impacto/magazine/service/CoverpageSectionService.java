package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersCoverpageSectionRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.CoverpageSectionResponse;

public interface CoverpageSectionService {
	public BaseResponse<CoverpageSectionResponse> getCoverpageSection(FiltersCoverpageSectionRequest request) throws Exception;
}
