package net.impacto.magazine.service;

import net.impacto.magazine.entity.Credentials;

public interface AccessService {
	public Credentials getCredentials() throws Exception;
}