package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;

public interface ContentService {
	public BaseResponse<?> insertContent(ContentRequest request) throws Exception;
	public BaseResponse<FileLink> transferImg(FileBase64 request) throws Exception;
}
