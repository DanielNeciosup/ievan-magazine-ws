package net.impacto.magazine.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.service.AccessService;
import net.impacto.magazine.service.FileServerService;
import net.impacto.magazine.service.FileTransferService;
import net.impacto.magazine.service.ResponseService;

@Service("fileServerService")
public class FileServerServiceImpl implements FileServerService {
	@Autowired
	private ResponseService responseService;
	
	@Resource(name = "accessFileServerService")
	private AccessService accessFileServerService;
	
	@Resource(name = "fileTransferSSHService")
	private FileTransferService fileTransferSSHService;
	
	@Override
	public BaseResponse<String> exportTo(String sourceFilePath, String targetFolderPath) throws Exception {
		BaseResponse<String> response = new BaseResponse<>();

		/**
		 * Export file to file server by SSH
		 */
		fileTransferSSHService.exportFileTo(accessFileServerService.getCredentials(), sourceFilePath, targetFolderPath);
		
		return responseService.onSuccess(response, sourceFilePath, null);
	}

	@Override
	public BaseResponse<String> importFrom(String sourceFilePath, String targetFolderPath) throws Exception {
		BaseResponse<String> response = new BaseResponse<>();

		return response;
	}
}
