package net.impacto.magazine.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.magazine.dao.TagDAO;
import net.impacto.magazine.dtos.request.FiltersTagsRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.ListTagResponse;
import net.impacto.magazine.entity.Item;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.service.TagService;

@Service("tagService")
public class TagServiceImpl implements TagService {
	@Autowired
	private ResponseService responseService;
	
	@Autowired
	private TagDAO tagDAO;

	public BaseResponse<ListTagResponse> listTags(FiltersTagsRequest request) throws Exception {
		BaseResponse<ListTagResponse> response = new BaseResponse<>();
		
		ListTagResponse data = new ListTagResponse();
		List<Item> listTag = tagDAO.listTags(request);
		data.setListTag(listTag);
		
		responseService.onSuccess(response, data, null);

		return response;
	}
}
