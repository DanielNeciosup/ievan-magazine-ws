package net.impacto.magazine.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.impacto.magazine.dao.EditionDAO;
import net.impacto.magazine.dtos.request.ContentRequest;
import net.impacto.magazine.dtos.request.EditionImageHomeRequest;
import net.impacto.magazine.dtos.request.EditionRequest;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.dtos.request.FiltersFileEditionRequest;
import net.impacto.magazine.dtos.request.ListConsolidatedRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.dtos.response.EditionResponse;
import net.impacto.magazine.dtos.response.ListEditionContentsResponse;
import net.impacto.magazine.dtos.response.ListEditionsResponse;
import net.impacto.magazine.dtos.response.ListFileEditionResponse;
import net.impacto.magazine.entity.Confirmation;
import net.impacto.magazine.entity.Consolidated;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;
import net.impacto.magazine.enums.ResponseApi;
import net.impacto.magazine.helpers.JSONHelper;
import net.impacto.magazine.params.FileServerRoutesParams;
import net.impacto.magazine.service.AccessService;
import net.impacto.magazine.service.EditionService;
import net.impacto.magazine.service.FileServerService;
import net.impacto.magazine.service.FileTransferService;
import net.impacto.magazine.service.ResponseService;
import net.impacto.magazine.utils.FileUtil;

@Service("editionService")
public class EditionServiceImpl implements EditionService{
	@Autowired
	private ResponseService responseService;
	
	@Resource(name = "fileTransferSSHService")
	private FileTransferService fileStorageService;
	
	@Resource(name = "accessFileServerService")
	private AccessService accessService;
	
	@Autowired
	private FileServerRoutesParams fsRoutesParams;
	
	@Autowired
	private FileServerService fileServerService;
	
	@Autowired
	private EditionDAO editionDAO; 
	
	
	@Override
	public BaseResponse<EditionResponse> insertEdition(EditionRequest request) throws Exception{
		BaseResponse<EditionResponse> response = new BaseResponse<>();
		EditionResponse data = new EditionResponse();
		FileLink imageHome = new FileLink();
		imageHome = transferImageToServer(request.getHomeImage()).getData();
		request.getEdition().setMainImage(imageHome);
		Confirmation confirmation = new Confirmation();
		confirmation = editionDAO.insertEdition(request).get(0);
		/* FALTA RETORNAR LA EDICION INSERTADA LiSTEDITION*/
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<EditionResponse> registerMainEdition(EditionRequest request) throws Exception{
		BaseResponse<EditionResponse> response = new BaseResponse<>();
		EditionResponse data = new EditionResponse();
		FileLink imageCoverpage = new FileLink();
		imageCoverpage = transferImageToServer(request.getCoverpageImage()).getData();
		request.getEdition().setCoverpage(imageCoverpage);
		Confirmation confirmation = new Confirmation();
		confirmation = editionDAO.insertEditionMainData(request).get(0);
		/* FALTA RETORNAR LA EDICION INSERTADA LiSTEDITION*/
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<FileLink> transferImageToServer(FileBase64 request ) throws Exception{
		BaseResponse<FileLink> response = new BaseResponse<>();
		FileLink data = new FileLink();
		File file = FileUtil.decodeToFile(request.getEncodeBase64(), request.getName());
		String originalName = file.getName();
		String newName = UUID.randomUUID().toString();
		String extension = FilenameUtils.getExtension(file.getName());
		file = FileUtil.rename(file, newName + "." + extension);
		String targetFolder = String.format("%s%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getImagesEditionsPath());
		fileServerService.exportTo(file.getPath(), targetFolder);
		data.setLink(String.format("%s/%s", fsRoutesParams.getImagesEditionsPath(), file.getName()));
		data.setName(FilenameUtils.removeExtension(originalName));
		FileUtil.delete(file.getPath());
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<?> updateConsolidatedEdition(ListConsolidatedRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		List<Consolidated> listPrev = request.getListConsolidated();
		for (Consolidated consolidated : listPrev) {
			FileLink datos = transferFileToServer(consolidated.getFileBase64(), consolidated.getName(), request.getLang()).getData();
			consolidated.setExtension(FilenameUtils.getExtension(consolidated.getName()));
			consolidated.setLink(datos.getLink());
			consolidated.setFileBase64("");
		}
		List<Consolidated> listAux = new ArrayList<>();
		for (Consolidated consolidated : listPrev) {
			if(consolidated.getOperationCode() == 3) {
				listAux.add(consolidated);
				listPrev.remove(consolidated);
			}
		}
		for (Consolidated consolidated : listPrev) {
			listAux.add(consolidated);
		}
		request.setListConsolidated(listAux);
		List<Confirmation> listConfirmations = editionDAO.updateConsolidated(JSONHelper.objectToJSONString(request));
		Confirmation confirmation = listConfirmations != null && !listConfirmations.isEmpty() ? listConfirmations.get(0) : null;
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				responseService.onSuccess(response, null, null);
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				listConfirmations.forEach(c -> {
					responseService.addBusinessError(response, c.getMessage());
				});
			}
		}
		return response;
	}
	
	@Override
	public BaseResponse<?> insertContentEdition(ContentRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		FileLink fileLink = transferImageToServer(request.getImage()).getData();
		request.getContent().setImage(fileLink);
		List<Confirmation> listConfirmations = editionDAO.insertContentEdition(request);
		Confirmation confirmation = listConfirmations != null && !listConfirmations.isEmpty() ? listConfirmations.get(0) : null;
		if (confirmation != null) {
			if (confirmation.getId() == ResponseApi.OK.getCode()) {
				responseService.onSuccess(response, null, null);
			} 
			else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()) {
				listConfirmations.forEach(c -> {
					responseService.addBusinessError(response, c.getMessage());
				});
			}
		}
		return response;
	}
	
	@Override
	public BaseResponse<FileLink> transferFileToServer(String fileBase64, String fileName, String codeLanguage) throws Exception{
		BaseResponse<FileLink> response = new BaseResponse<>();
		FileLink data = new FileLink();
		File file = FileUtil.decodeToFile(fileBase64 , fileName);
		String originalName = file.getName();
		String newName = FilenameUtils.removeExtension(originalName) + "_"+ UUID.randomUUID().toString();
		String extension = FilenameUtils.getExtension(file.getName());
		file = FileUtil.rename(file, newName + "." + extension );
		String targetFolder = String.format("%s%s/%s/%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getConsolidatedEditionsPath(), extension, codeLanguage);
		fileServerService.exportTo(file.getPath(), targetFolder);
		data.setLink(String.format("%s/%s/%s/%s",fsRoutesParams.getConsolidatedEditionsPath(), extension, codeLanguage, file.getName() ));
		data.setName(FilenameUtils.removeExtension(originalName));
		FileUtil.delete(file.getPath());
		return responseService.onSuccess(response, data, null);
	}
	
	

	
	////////////////////////////////////////////SEPARADOR///////////////////////////////////////////////
	

	@Override
	public BaseResponse<FileLink> updateImageHome(FileBase64 request) throws Exception{
		BaseResponse<FileLink> response = new BaseResponse<FileLink>();
		FileLink file = new FileLink();
		//file = transferCoverpageToServer(request.getFile()).getData();
		EditionImageHomeRequest requestDAO = new EditionImageHomeRequest();
		requestDAO.setFile(file);
		//requestDAO.setEditionId(request.getEditionId());
		response.setData(file);
		return response;
	}
	

	
	@Override
	public BaseResponse<EditionResponse> updateEditions(EditionRequest request) throws Exception{
		BaseResponse<EditionResponse> response = new BaseResponse<>();
		
		EditionResponse data = new EditionResponse();
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<ListEditionsResponse> listEditions(FiltersEditionsRequest request) throws Exception{
		BaseResponse<ListEditionsResponse> response = new BaseResponse<>();
		ListEditionsResponse data = new ListEditionsResponse();
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<ListFileEditionResponse> listFilesEditions(FiltersFileEditionRequest request) throws Exception{
		BaseResponse<ListFileEditionResponse> response = new BaseResponse<>();
		ListFileEditionResponse data = new ListFileEditionResponse();
		return responseService.onSuccess(response, data, null);
	}
	/*

	@Override
	public BaseResponse<ListEditionsResponse> insertEdition(EditionRequest request) throws Exception{
		BaseResponse<ListEditionsResponse> response = new BaseResponse<>();
		ListEditionsResponse data = new ListEditionsResponse();
		List<Consolidated> listConsolidated = new ArrayList<Consolidated>();
		Consolidated cons = new Consolidated();
		if(request.getListCons() != null) {
			for (Consolidated file : request.getListCons()) {
				cons.setFileConsolidated(transferFileToServer( file.getFileConsolidated64() , request.getEdition().getLanguage().getCode() ).getData());
				cons.setImage(transferCoverpageToServer( file.getImage64() , request.getEdition().getLanguage().getCode()).getData());				
				listConsolidated.add(cons);
			}
		}
		FileLink imageHome = new FileLink();
		imageHome = transferCoverpageToServer(request.getImage() , request.getEdition().getLanguage().getCode()).getData();
		request.getEdition().setHomeImage(imageHome);
		
		var listConfirmation = editionDAO.insertEdition(request);
		FiltersEditionsRequest filtersRequest = new FiltersEditionsRequest();
		filtersRequest.setEditionIds(listConfirmation.get(0).getDataId().toString());
		filtersRequest.setIdLanguage(request.getEdition().getLanguage().getCode());
		FiltersEditionFormatLanguage fil = new FiltersEditionFormatLanguage();
		fil.setIdEdition(listConfirmation.get(0).getDataId());
		FiltersEditionLanguage fel = new FiltersEditionLanguage();
		fel.setIdEdition(listConfirmation.get(0).getDataId());
		
		if(listConsolidated != null) {
			for (Consolidated consolid : listConsolidated) {
				fel.setTxtImgCoverpageLink(consolid.getImage().getLink());
				fel.setTxtImgCoverpageName(consolid.getImage().getName());
				
				editionDAO.insertEditionFormatLanguage(fil);
				editionDAO.insertEditionLanguage(fel);
			}
		}
		data = listEditions(filtersRequest).getData();
		data.setConsolidates(listConsolidated);
		return responseService.onSuccess(response, data, null);
	}
	
	@Override
	public BaseResponse<FileLink> transferFileToServer(FileBase64 request, String codeLanguage) throws Exception{
		BaseResponse<FileLink> response = new BaseResponse<>();
		FileLink data = new FileLink();
		
		File file = FileUtil.decodeToFile(request.getEncodeBase64(), request.getName());
		String originalName = file.getName();
		String newName = FilenameUtils.removeExtension(originalName) + "_"+ UUID.randomUUID().toString();
		String extension = FilenameUtils.getExtension(file.getName());
		file = FileUtil.rename(file, newName + "." + extension );
		String targetFolder = String.format("%s%s/%s/%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getConsolidatedEditionsPath(), extension, codeLanguage);
		fileServerService.exportTo(file.getPath(), targetFolder);
		
		data.setLink(String.format("%s/%s/%s/%s",fsRoutesParams.getConsolidatedEditionsPath(), extension, codeLanguage, file.getName() ));
		data.setName(FilenameUtils.removeExtension(originalName));

		FileUtil.delete(file.getPath());
		
		return responseService.onSuccess(response, data, null);
	}

	@Override
	public BaseResponse<FileLink> transferCoverpageToServer(FileBase64 request, String language) throws Exception{
		BaseResponse<FileLink> response = new BaseResponse<>();
		FileLink data = new FileLink();
		
		File file = FileUtil.decodeToFile(request.getEncodeBase64(), request.getName());
		String originalName = file.getName();
		String newName = UUID.randomUUID().toString();
		String extension = FilenameUtils.getExtension(file.getName());
		file = FileUtil.rename(file, newName + "." + extension);
		String targetFolder = String.format("%s%s", fsRoutesParams.getMultimediaPath(), fsRoutesParams.getImagesEditionsPath());
		fileServerService.exportTo(file.getPath(), targetFolder);
		data.setLink(String.format("%s/%s", fsRoutesParams.getImagesEditionsPath(), file.getName()));
		data.setName(FilenameUtils.removeExtension(originalName));
		FileUtil.delete(file.getPath());
		return responseService.onSuccess(response, data, null);
	}
*/
	
	
	@Override
	public BaseResponse<ListEditionContentsResponse> listEditionContents(FiltersEditionsRequest request) throws Exception{
		BaseResponse<ListEditionContentsResponse> response = new BaseResponse<>();
		ListEditionContentsResponse data = new ListEditionContentsResponse();
		//var listContents = contentDAO.listContents(request);
		responseService.onSuccess(response, data, null);
		return response;
	}
	
	@Override
	public BaseResponse<ListEditionContentsResponse> listLastEditionContents() throws Exception{
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		BaseResponse<ListEditionContentsResponse> response = new BaseResponse<>();
		ListEditionContentsResponse data = new ListEditionContentsResponse();
		request.setEditionIds(null);
		request.setIsLast(true);
		request.setPageNum(1);
		responseService.onSuccess(response, data, null);
		return response;
	}
}