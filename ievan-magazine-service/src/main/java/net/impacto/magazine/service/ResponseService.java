package net.impacto.magazine.service;

import net.impacto.magazine.dtos.request.FiltersPaginationRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.entity.Pagination;

public interface ResponseService {
	public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination, String message);

    public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination);

    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response, String message);

    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response);

    public <T> BaseResponse<T> addBusinessError(BaseResponse<T> response, String message);
    
    public <T> Pagination calculatePagination(boolean isEmptyList, FiltersPaginationRequest fpgRequest, Integer total, String uriWebService);
}
