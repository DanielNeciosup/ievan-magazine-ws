package net.impacto.magazine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanMagazineServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(IevanMagazineServiceApplication.class, args);
	}
}
