package net.impacto.magazine.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.config.AppServiceConfig;
import net.impacto.magazine.dtos.request.FiltersEditionsRequest;
import net.impacto.magazine.entity.Language;
import net.impacto.magazine.entity.Zone;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class ZoneServiceTest {
	@Autowired
	private ZoneService zoneService;
	
	@Test
	public void listZoneLanguageEditionTest() {
		FiltersEditionsRequest req = new FiltersEditionsRequest();
		req.setEditionIds("1");
		List<Zone> zones ;
		try {
			zones = zoneService.listLanguageZonesEditions(req).getData().getListZone();
			for (Zone zone : zones) {
				System.out.println(zone.getId() + zone.getName());
				for (Language language : zone.getListLanguagesUsed()) {
					System.out.println(language.getId() + language.getName());
				}
				for (Language language : zone.getListLanguageNotUsed()) {
					System.out.println(language.getId() + language.getName());
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}


