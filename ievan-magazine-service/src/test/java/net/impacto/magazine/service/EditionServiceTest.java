package net.impacto.magazine.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.config.AppServiceConfig;
import net.impacto.magazine.dtos.request.FileBase64EditionLanguageRequest;
import net.impacto.magazine.entity.FileBase64;
import net.impacto.magazine.entity.FileLink;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class EditionServiceTest {
	@Autowired
	//private EditionService editionService;
	
	@Test
	public void updateImageHomeTest() {
		FileBase64 file = new FileBase64();
		FileLink response = new FileLink();
		file.setEncodeBase64("/link pruba service NOW");
		file.setName("nombre de imagen home prueba service NOW");
		FileBase64EditionLanguageRequest request = new FileBase64EditionLanguageRequest();
		request.setFile(file);
		request.setEditionId(2);
		try {
			System.out.println(response.getLink() + response.getName());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	@Test
		public void listFileEditionServiceTest() {
			FiltersFileEditionRequest request = new FiltersFileEditionRequest();
			ListFileEditionResponse response = new ListFileEditionResponse();
			request.setEditionId(1);
			request.setLanguage("es");
			try {
					response = editionService.listFilesEditions(request).getData();
					response.getListFileEdition().forEach(file -> {
						System.out.println(file.getLinkConsolidated() + file.getFileType().getExtension());
					});
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	@Test
	public void insertEditionTest() {
		EditionRequest request = new EditionRequest();
		Edition edition = new Edition();
		Language language = new Language();
		FileBase64 imag = new FileBase64();
		FileBase64 file = new FileBase64();
			imag.setName("imagennombre.jpg");
			imag.setEncodeBase64("codigo base 64 imagen");
			file.setEncodeBase64("codigo base 64 archivo");
		FileBase64 imagenHome = new FileBase64();
		imagenHome.setName("imagen del home.jpg");
		imagenHome.setEncodeBase64("codigo imagen 64 portada");
		
		
		try {
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Test
	public void listEditionsTest() {
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		ListEditionsResponse response = new ListEditionsResponse();
		request.setPageNum(1);
		request.setIsLast(false);
		request.setPageSize(10);
		request.setEditionIds(null);
		request.setSortFilters("-id");
		request.setIdLanguage("es");
		try {
			response = editionService.listEditions(request).getData();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void listContentsTest() {
		FiltersEditionsRequest request = new FiltersEditionsRequest();
		ListEditionContentsResponse response = new ListEditionContentsResponse();
		request.setEditionIds("1");
		try {
			response = editionService.listEditionContents(request).getData();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void listLastContentsTest() {
		ListEditionContentsResponse response = new ListEditionContentsResponse();
		try {
			response = editionService.listLastEditionContents().getData(); 
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	*/
}


