package net.impacto.magazine.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.config.AppServiceConfig;
import net.impacto.magazine.dtos.request.FiltersSectionsRequest;
import net.impacto.magazine.dtos.response.ListSectionsResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class SectionServiceTest {
	@Autowired
	private SectionService sectionService;
	
	@Test
	public void listSectionsTest() {
		FiltersSectionsRequest request = new FiltersSectionsRequest();
		ListSectionsResponse response = new ListSectionsResponse();
		
		try {
			response = sectionService.listSections(request).getData();
			response.getListSections().forEach( section -> {
				System.out.println( section.getName() );
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
