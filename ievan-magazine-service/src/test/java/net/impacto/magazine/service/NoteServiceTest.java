package net.impacto.magazine.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.magazine.config.AppDAOConfigTest;
import net.impacto.magazine.config.AppServiceConfig;
import net.impacto.magazine.dtos.request.ListTagsNoteRequest;
import net.impacto.magazine.dtos.response.BaseResponse;
import net.impacto.magazine.entity.Item;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class NoteServiceTest {
	@Autowired
	private NoteService noteService;
	
	@Test
	public void updateTagsTest()
	{
		var request = new ListTagsNoteRequest();
		
		Item tag1 = new Item();
		tag1.setId(4);
		tag1.setOperationCode(3);
		
		Item tag2 = new Item();
		tag2.setId(5);
		tag2.setOperationCode(1);
		
		List<Item> listTags = new ArrayList<>();
		listTags.add(tag1);
		listTags.add(tag2);
		
		request.setNoteId(33);
		request.setListTags(listTags);
		
		try 
		{
			BaseResponse<?> response = noteService.updateTags(request);
			System.out.println(String.format("id:%s, message: %s", response.getState(), response.getMessage()));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
